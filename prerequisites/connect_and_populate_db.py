### Creates and populates a new database with the inital URLs list.
### This file should only run once at the start of the program and once
### during the NLP  phase ???

### TODO###
# Shutdown.py - should close session, shut down tor.exe etc
# Take in parameters cause of phase 2?

### IMPORTS ###
from pymongo import MongoClient
from pymongo.collation import Collation

from datetime import date, datetime


# TODO: start mongodb as a service

collection = {
    'locale': 'en_US',
    'strength': 2,
    'numericOrdering': True,
    'backwards': False
}

#create_collection(collection)
client = MongoClient('mongodb://localhost:27017/')
db = client["DWProject"]
col = ["DW_URLs"]


# Checks if db exists, if not creates new one
if "DWProject" in client.list_database_names():
    pass

# Checks if collection exists, if not creates new one
collist = db.list_collection_names()
if "DWURLs" in collist:
    pass

## else run this

# Copies all initial urls to the URLs list to be added to the database
urls = []
with open('prelim_links.txt', 'r') as pl:
    for l in pl:
        urls.append(l)


### Populates the new database with the inital URLs list if it's empty
# Adds all URLs witht he default data to a dict to be psuhed to the dbn
entries = []
for url in urls:
    entries.append({
        'url': url.strip(),
        'title': None,
        'redirects': [],
        'dateLastChecked': None,
        'parent_sites': [],
        'status': None, # online, offline, unknown
        'topics': [],
        'data': [],
        'categories': [], # set my ml
        'relevant': None,
        'visited': False,
        'username': "",
        'password': "",
    })


client = MongoClient('mongodb://localhost:27017/')
db = client["DWProject"]["DW_URLs"]

db.insert_many(entries)
db.create_index('url',  unique=True)

client.close()

