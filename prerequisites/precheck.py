import os
import subprocess
import sys
import win32serviceutil
import win32com.shell.shell as shell
import win32service as ws
import psutil

import win32api, win32con, win32event, win32process
from win32com.shell.shell import ShellExecuteEx
from win32com.shell import shellcon

### TODO:
# Start up TOR
# Start up pproxy
# Make sure mongodb setup

found = False
# Checks to see if python proxy is already running
for p in psutil.process_iter():
    if "pproxy.exe" == p.name():
        found = True

if not found:
    # Starts Proxy
    try:
        subprocess.Popen('pproxy -l http://:8181 -r socks5://127.0.0.1:9050 -vv')  # TODO: get port
    except Exception as e:
        print("An error occurred while trying to start the proxy:", e)
    else:
        print("Proxy started, listening on port 8181")
else:
     print("Proxy already listening on port 8181")


# Checks to see if Tor is already installed as a service, if not attempts to install it
try: 
    win32serviceutil.QueryServiceStatus('tor')
except:
    
    try:
        # Runs as admin
        procInfo = ShellExecuteEx(nShow=win32con.SW_HIDE,
                                  fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
                                  lpVerb='runas',
                                  lpFile='..\\..\\tor-win32-0.4.4.6\\Tor\\tor.exe',
                                  lpParameters='--service install')

    except Exception as e:
        print("The Tor service must be installed in order to use this program.")
        print(e)
        i = input("Please press the Enter key to exit the program...")
    else:
        print("Tor service started successfully")
        
else:
    print("Tor service already installed")


# Checks to 

if win32serviceutil.QueryServiceStatus('tor')[1] == 4:
    print("Tor service already running")
else:
    # Starts Tor
    try:
        # Runs as admin
        procInfo = ShellExecuteEx(nShow=win32con.SW_HIDE,
                                  fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
                                  lpVerb='runas',
                                  lpFile='net',
                                  lpParameters='start tor')

    except Exception as e:
        print("Tor service must be running in order to use this program.")
        print(e)
    else:
        print("Tor service started successfully")
