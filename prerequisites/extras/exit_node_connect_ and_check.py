### TODO###
# Shutdown.py - should close session, shut down tor.exe etc

### IMPORTS ###
import configparser
import json
import requests
import os
import subprocess

class Error(Exception):
    """Base class for other exceptions"""
    pass

class FailedToStartTor(Error):
    """Raised when tor.exe fails to start"""
    pass

### START ###
try:
    # Attempts to starts the tor.exe processs
    config = configparser.ConfigParser()
    config.read('config.ini')
    if 'TOR' not in config:
        print("""TODO: Tell people should look like this, fix it, etc.
[TOR]
service_location="../../tor-win32-0.4.4.6/Tor/tor.exe"
port=9050
        """)
        
    path = config['TOR']['service_location']
    subprocess.run(path)

    #os.system('service tor.exe stop')

    if os.system('service tor.exe status') == 0:
        print("tor.exe was started sucessfully on port 9050")
    else:
        print("Failed to start tor.exe (The service NOT the browser).")
        raise FailedToStartTor

except FailedToStartTor:
    print("The tor.exe service could not be started. Please ensure the congig.ini file is set up correctly. etc")

except Exception as e:
    print("An exception occcured while trying to start the tor.exe process")
    print("Exception: {}".format(e))
        

print("Successfully connected to the Tor network.")

# Establishes a new session
session = requests.session()

# When using tor.exe (headless)
# NOTE: If using the TOR browser, replace 9050 with 9150
session.proxies = {
    'http': 'socks5h://127.0.0.1:9050',
    'https': 'socks5h://127.0.0.1:9050'
}

# NOTE: URL test site (duckduckgo): https://3g2upl4pq6kufc4m.onion/
# Attempts to see if a conncetion to a known safe hidden service can be established
#https://docs.python.org/3.8/library/configparser.html
try:
    #r = session.get('https://3g2upl4pq6kufc4m.onion/')
    pass
except Exception as err:
    print(err)

print("Successfully connected to the Tor network.")
session.close()
