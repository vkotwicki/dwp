### TODO ###
# Rotate user agents every few connectiosn
# Selenium captcha solver
# crawl URL child Urls and save both to DB
# Stagger for 1-3 seconds


# TODO: add new urls to db with their info + save parent cite
# TODO: pull URLs from db plus set if they have been visited or not
# TODO: make sure no dupes in bdb, just uipdate parent site list??
# TODO: get headers and key words too??

### TECHS
# Multi thread + multi process


# https://darkweburls.com/
# https://cybarrior.com/blog/2019/03/28/huge-collection-of-deep-web-onion-links/
from pymongo import MongoClient
#from fake-useragent import UserAgent

from urllib.parse import urljoin
from bs4 import BeautifulSoup

import configparser
import json

import os
import subprocess
import re

import selenium
import requests

### DEBUGGING
import logging
import time

logging.basicConfig(
    format='%(asctime)s %(levelname)s:%(message)s',
    level=logging.INFO)


### GLOBAL VARS
#ua = UserAgent() #cache=False


# Adds entries in batches
def add_to_db(entries):
    # TODO: see if db exists, if not insert dud data
    client = MongoClient('mongodb://localhost:27017/')
    db = client["DWProject"]
    coll = db["DW_URLs"]
    data = {
        'url': url.strip(),
        'dateLastChecked': dt,
        'parent_sites': [],
        'status': 'unknown', # online, offline, unknown
        'topics': [],
        'data': [],
        'Categories': [], # set my ml
        'relevant': False,
        'visited': False,
    }
    coll.insert_many(entries)
    client.close()


# https://docs.python.org/3.8/library/configparser.html

# Replicates clean pulled URLs
db_list = {}

### TODO: every x crawls restart agent?

# for now get from prelim links
urls = []
def pull_URL_batch():
    with open('prelim_links.txt', 'r') as file:
        for l in pl:
            urls.append(l)

            

### TODO:
# - CHECK FOR DUPLICATE ENTRIES AND UPDATE THEM
# - Update parent URL to say visited

session = requests.session()
client = MongoClient('mongodb://localhost:27017/')
db = client["DWProject"]
coll = db["DW_URLs"]

class Crawler:
    # When using tor.exe (headless)
    # NOTE: If using the TOR browser, replace 9050 with 9150
    
    session.proxies = {
        'http': 'socks5h://127.0.0.1:9050',
        'https': 'socks5h://127.0.0.1:9050'
    }

    batch_entries = []

    #Replace with get from db
    def __init__(self, urls=[]):
        self.visited_urls = get_URL_batch()
        self.urls_to_visit = urls


    # Returns a batch of 1000 URLs
    def get_URL__batch(self):
        url_batch = coll.find({}, {'visited': False}).limit(1000) # This may or may not cause issues as the size of the db increases
        print(url_batch)
        time.sleep(100)
        return url_batch


    # Adds new urls to db
    def add_to_db(self, e):
        coll.insert_many(e)
        self.batch_entries = []
        return
    

    # Gets the page's HTML
    def get_HTML(self, url): #header
        try:
            res = session.get(url).text
            #url_batch{} ### TODO: update if up or not, remove from batch if not
        except Exception as err:
            print(err)
            res = None

        return res

            
    # Generates a new header for the request
    def new_header(self):
        user_agent = ua.random
        header = {}
        header['User-agent'] = "HotJava/1.1.2 FCS"
        # code to randomise header from csv etc
        return header


    def get_onion_urls(self, parent_url, html):
        titles_headers = []

        # gets onion URLS from website
        try:
            url_16 = re.compile(r'[a-zA-Z2-7]{16}\.onion', re.DOTALL | re.MULTILINE)
            url_56 = re.compile(r'[a-zA-Z2-7]{56}\.onion', re.DOTALL | re.MULTILINE)
            urls = re.findall(url_16, html)
            urls.extend(re.findall(url_56, html))
            urls = list(set(urls))
            
            print("Found URLs: ", url)

        except Exception as e:
            print(f'Exception occured: {e}')

        # Gets headings and title
        soup  = BeautifulSoup(html, 'html.parser')
        titles_headers.append(soup.find_all(['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'title']))

        print("")
        print("HEADINGS: ", titles_headers)

        for url in urls:
            batch_entries.append({
                'url': url.strip(),
                'dateLastChecked': dt,
                'parent_sites': parent_url, # TODO: check if already in db in the add bit
                'status': 'unknown', # online, offline, unknown
                'topics': [],
                'titles': titles_headers,
                'categories': [], # set my ml
                'relevant': False,
                'visited': False,
            })
            

        if len(urls) > 1000:
            add_to_db(urls)
            urls = []

        time.sleep(100)



    def add_url_to_visit(self, url):
        if url not in self.visited_urls and url not in self.urls_to_visit:
            self.urls_to_visit.append(url)


    def crawl(self, url):
        html = self.get_HTML(url)
        for url in self.get_onion_urls(html):
            self.add_url_to_visit(url)

    def run(self):
        
        while self.urls_to_visit:
            url = self.urls_to_visit.pop(0)
            logging.info(f'Crawling: {url}')
            try:
                self.crawl(url)
            except Exception:
                logging.exception(f'Failed to crawl: {url}')
            finally:
                self.visited_urls.append(url)

if __name__ == '__main__':
    Crawler(urls=['http://www.propub3r6espa33w.onion/']).run()
    session.close()
    client.close()









