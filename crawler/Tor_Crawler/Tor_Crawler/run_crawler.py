from pymongo import MongoClient
from pymongo.collation import Collation

import os, sys
import subprocess


client = MongoClient('mongodb://localhost:27017/')
db = client["DWProject"]["DW_URLs"]


count = 0
while count < 2000:
    # TODO: Clear console
    subprocess.call('cls', shell=True)
    visisted_sites = db.find({'visited': True}).count()
    db_urls = db.find({}).count()
    urlsCheck = list(db.find({'visited': False}, {'url': 1}).limit(1))
    print(visisted_sites, " webpages processed")
    print(db_urls, " urls in database")

    if len(urlsCheck) == 0:
        print("End of database reached")
        break

    # Hides console
    si = subprocess.STARTUPINFO()
    si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    si.wShowWindow = subprocess.SW_HIDE
    subprocess.call('scrapy crawl tc', startupinfo=si)

    count += 1
    
client.close()
print("owo")
