# TODO: User agent
# TODO: Pillow & pytesseract -> deal with robot checks + selenium
# TODO: reset Tor route every x requ
# TODO: implement robot check
# TODO: auto signup with fake email? -> selenium
# TODO: auto username and password craetion + sign in
# e.g. zw3crggtadila2sg.onion/imageboard/ is torchan, needs username and password torchan3
# costeira.i2p.onion not picked up

# TODO: write all this up in the report

# TODO: update db with http or https programmatically
import scrapy
from pymongo import MongoClient
from datetime import datetime
#from scrapy.http import FormRequest
from scrapy.crawler import CrawlerProcess
from scrapy.spidermiddlewares.httperror import HttpError
from scrapy.core.downloader.handlers.http11 import TunnelError
from twisted.internet.error import TimeoutError
from twisted.web._newclient import ResponseNeverReceived
import sys


class TorCrawler(scrapy.Spider):
    name = "tc"
    handle_httpstatus_list = [404, 502, 503, 504]

    num_processed = 1

    def __init__(self):
        self.client = MongoClient('mongodb://localhost:27017/')
        self.db = self.client["DWProject"]["DW_URLs"]

    # TODO: restart polipo if it breaks
    def start_requests(self):
        count = 0
        urls = self.db.find({'visited': False}, {'url': 1}).limit(1)

        for u in urls:
            url = u['url']
            if "http://" not in url and "https://" not in url:
                new_url = "http://" + url
                self.db.update_one(
                    {'url': url},
                    {
                        '$set': {
                            'url': new_url
                        }
                    }
                )
                url = new_url

            yield scrapy.Request(url=url,
                                 callback=self.parse,
                                 errback=self.errback_url,
                                 cb_kwargs=dict(og_url=url),
                                 meta={'dont_retry': True,
                                       'download_timeout': 15,
                                       'dont_filter': True},
                                 # headers={'User-Agent': 'owo'})  # TODO: get new ones each new batch
                                 )
            count += 1
            # print(count, "urls passed")

    # TODO: end spider
    def parse(self, response, og_url):
        # Returns the site's url, the url it was redirected too if applicable and the site's raw html
        if response.status >= 500:
            yield {
                'url': og_url,
                'redirects': "",
                'response': "",
                'status': response.status
            }
        else:
            yield {
                'url': og_url,
                'redirects': response.request.url,
                'response': response,
                'status': response.status
            }

    def errback_url(self, error):
        url = ""
        status = 0
        if error.check(HttpError):
            # these exceptions come from HttpError spider middleware
            # you can get the non-200 response

            res = error.value.response

            newurl = res.url
            url = newurl
            #print("Url: ", url)
            #print("Error value: ", error.value.response)
            #print("Url: ", url)

            err_res = str(error.value.response)
            err_list = err_res.replace('<', '').replace('>', '').split(' ')
            # print(err_list)
            status = err_list[0]
            status = 400

        elif error.check(TimeoutError):
            # print("ERROR TIME OUT")
            url = error.request.url
            status = 408

        elif error.check(ResponseNeverReceived):
            # ("ERROR RES NEVER RECEIVED")
            url = error.request.url
            status = 503

        elif error.check(TunnelError):
            # tends to be that url accessed was accessed as a https site when it's not
            # print("ERROR TUNNEL ERROR")
            url = error.request.url
            status = 504

        else:
            #print("OTHER ERROR")
            #print("Error type: ", error.type)
            try:
                url = error.response.url
            except:
                pass
            try:
                url = error.value.url
            except:
                pass
            try:
                url = error.url
            except:
                pass
                #print("no url")

            status = 500

        b = False

        if "https://" in url:
            b = True
        elif "http://" in url:
            new_url = url.replace('http://', 'https://')
            self.db.update_one(
                {'url': url},
                {
                    '$set': {
                        'url': new_url,
                        'dateLastChecked': datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
                    }
                }
            )
            url = new_url

        self.db.update_one(
            {'url': url},  # Where item is the URL
            {
                '$set': {
                    'status': status,
                    'dateLastChecked': datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
                    'visited': b
                }
            },
        )

    def closed(self, reason):
        self.client.close()
        sys.exit()
