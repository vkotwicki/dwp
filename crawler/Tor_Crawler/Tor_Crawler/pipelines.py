# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

# TODO: Login -> need tor email
# TODO: Categoriser key words list
# TODO: ml

from pymongo import MongoClient
from datetime import datetime
from bs4 import BeautifulSoup

import re
import time
import json


class TorCrawlerPipeline:
    def __init__(self):
        self.entry = {
            'url': None,  # url.strip(),
            'title': None,  # For quickly recognising website without visisting
            'redirects': [],
            'dateLastChecked': None,  # dt
            'parent_sites': [],
            'status': None,  # online, offline, unknown
            'topics': [],
            'data': [],
            'categories': [],  # set my ml
            'relevant': None,
            'visited': False,
            'username': "",
            'password': ""
        }

    def open_spider(self, spider):
        self.client = MongoClient('mongodb://localhost:27017/')
        self.db = self.client["DWProject"]["DW_URLs"]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        # Adds/Updates URL entry to/in DB

        def captcha_solver():
            pass

        def captcha_detector():
            # TODO: download async to wait for results?
            pass

        def update_url():
            # TODO: check if in db (parent or redirect) to update list of parent sites
            # TODO: not recording parent sites for now or redirects

            self.db.update_one(
                {'url': db_url},  # Where item is the URL
                {
                    # '$addToSet': {'redirects': item['redirects']},  # Update entry if not already in there
                    '$set': {
                        'url': item['url'],
                        'title': title,
                        'status': item['status'],
                        'dateLastChecked': datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
                        'data': cleaned_data,
                        'visited': True
                    }
                },
            )

        # Adds newly found urls to the db
        def add_new_urls():
            # TODO: check if already in db and if it is, don't upload
            # print(len(new_urls), "urls to add.")
            for u in new_urls:
                # TODO: make existing function work with this
                url1 = u.replace('https://', '').replace('http://', '')  # try without protocol
                url2 = "http://" + u  # try with http protocol
                url3 = "https://" + u

                check1 = self.db.find_one({'url': url1}, {"_id": 1})  # checks if url without http(s) exists
                check2 = self.db.find_one({'url': url2}, {"_id": 1})  # checks if url with http exists
                check3 = self.db.find_one({'url': url3}, {"_id": 1})  # checks if url with https exists

                # Checks to see if URL already int the database as mongodb has removed this functionality
                if check1 is None and check2 is None and check3 is None:
                    # If it's not, attempts to add it to the database
                    try:
                        self.db.insert_one({
                            'url': u,
                            'visited': False
                        })
                    except Exception as e:
                        # print("EXCEPTION OCCURRED WHILE TRYING TO INSERT URL INTO DB: ", e)
                        pass
                    else:
                        pass
                        # print("added url: ", u)

        # Pulls complete URLs from the html of the web page
        # TODO: search for ?q as well
        # e.g. http://kx5thpx2olielkihfyo4jgjqfb7zx7wxr3sd4xzt26ochei4m6f7tayd.onion/
        def get_urls():
            urls = []
            if item['response'] == '':
                return urls

            soup = BeautifulSoup(item['response'], "html.parser")

            for a in soup.find_all('a', href=True):
                # print("Found the URL:", a['href'])
                if a['href'] != "#":
                    if "http://" in a['href'] or "https://" in a['href']:
                        if ".onion" in a['href']:
                            ul = a['href']
                        else:
                            continue
                    else:
                        if a['href'].startswith('/') or a['href'].startswith('\\'):
                            ul = str(item['url']) + str(a['href'])

                        else:
                            ul = str(item['url']) + '/' + str(a['href'])

                    urls.append(ul)
                else:
                    pass
                    #print("Interactive")

            soup = soup.get_text()

            # Can find links BS4 can't
            url_16 = re.compile(r'(?:\bhttp://\b|\bhttps://\b)*(?:[a-zA-Z0-9-_][\.]*)*[a-zA-Z2-7]{16}\.onion(?:[./:#?=]+[a-zA-Z0-9-]*)*')  # , re.DOTALL | re.MULTILINE
            url_56 = re.compile(r'(?:\bhttp://\b|\bhttps://\b)*(?:[a-zA-Z0-9-_][\.]*)*[a-zA-Z2-7]{56}\.onion(?:[./:#?=]+[a-zA-Z0-9-]*)*')

            urls.extend(re.findall(url_16, soup))
            # print(urls)
            # print("")
            urls.extend(re.findall(url_56, soup))
            # print(urls)
            # print("")

            temp = []
            for u1 in urls:
                temp.append(u1.strip())

            urls = temp

            urls = list(set(urls))
            # print(urls)
            # TODO: remove images
            # print("urls")
            count = 0
            for i in urls:
                if i[-1] == ".":
                    urls[count] = i[0:-1]  # removes fullstops at end of string from list
                count += 1


            return urls

        # Gets all data from website
        def parse_data():
            # TODO: do without BS4?
            title = ""
            data = []

            # title = item['response'].css('*::title')
            try:
                soup = BeautifulSoup(item['response'], "html.parser")
            except:
                return [title, data]

            try:
                title = soup.find("title").get_text()
            except:
                pass

            try:
                soup = soup.get_text()

                rmv_space = re.compile(r'\s{2,}', flags=re.UNICODE)  # \s\s+
                # Remove excessive white space and newline characters
                soup = (re.sub(rmv_space, '', soup))
                soup = soup.replace('\n', ' ')

                if soup != "":
                    soup_list = soup.split(' ')
                    data = list(set(soup_list))  # Removes duplicate words from the list because for categorising only

            except:
                pass

            return [title, data]

        def find_db_url():
            url = item['url']
            url1 = url.replace('https://', '').replace('http://', '') # try without protocol
            url2 = "http://" + url # try with http protocol
            url3 = "https://" + url

            check1 = self.db.find_one({'url': url1}, {"_id": 1})  # checks if url without http(s) exists
            check2 = self.db.find_one({'url': url2}, {"_id": 1})  # checks if url with http exists
            check3 = self.db.find_one({'url': url3}, {"_id": 1})  # checks if url with https exists

            if check1 is not None:
                url = url1
            elif check2 is not None:
                url = url2
            elif check3 is not None:
                url = url3

            return url


        if item['url'] != "":
            db_url = find_db_url()  # url that's currently in the database

            # print("PIPELINE URL:", item['url'])  # DEBUG
            res = item['response']
            try:
                item['response'] = item['response'].text
            except Exception:
                item['response'] = ''

            # Finally time url will be pulled
            if 400 <= item['status']:
                # print("https replace")
                if "https://" in item['url']:
                    # print("https")
                    self.db.update_one(
                        {'url': db_url},  # Where item is the URL
                        {
                            '$set': {
                                'status': item['status'],
                                'dateLastChecked': datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
                                'visited': True
                            }
                        },
                    )

                elif "http://" in item['url']:
                    # print("http replace")
                    # tries to connect to the url again by saving it as https
                    # If the url had no protocol, automatically tries http first so there's no need to
                    # check for that one as well here
                    new_url = item['url'].replace("http://", "https://")
                    self.db.update_one(
                        {'url': db_url},  # Where item is the URL
                        {
                            '$set': {
                                'url': new_url,
                                'status': item['status'],
                                'dateLastChecked': datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
                                'visited': False
                            }
                        },
                    )

            else:
                # If response successful
                # print("SUCESSFUL")
                title, cleaned_data = parse_data()
                new_urls = get_urls()

                for i in new_urls:
                    pass
                    # print("URL: ", i)

                add_new_urls()
                update_url()
        else:
            pass
            # print("OWO")

        return item
