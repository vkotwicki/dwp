from pymongo import MongoClient


client = MongoClient('mongodb://localhost:27017/')
db = client["DWProject"]["DW_URLs"]

print("Duplicate URLS:")

d = db.aggregate([
    {
        '$group': {
            '_id': '$url', 
            'dups': {
                '$push': '$_id'
            }, 
            'count': {
                '$sum': 1
            }
        }
    }, {
        '$match': {
            'count': {
                '$gt': 1
            }
        }
    }, {
        '$sort': {
            'count': -1
        }
    }
]);


for i in d:
    print(i)
