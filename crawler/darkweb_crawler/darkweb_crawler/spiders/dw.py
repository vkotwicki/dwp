import scrapy

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class DWCrawler(CrawlSpider):
    name = 'dw'
    
    allowed_domains = ['onion']
    #start_urls = ['https://www.imdb.com/']
    #rules = (Rule(LinkExtractor()),)

    def start_requests(self):
        urls = [
            'https://check.torproject.org/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url.split("/")[-2]
        filename = f'quotes-{page}.html'
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log(f'Saved file {filename}')
