data = """<a class="brief" href="//projects.propub3r6espa33w.onion/coronavirus-contracts">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/2020-0527-contracts-app-500x500-pointer.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne75w/2020-0527-contracts-app-500x500-pointer.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/2020-0527-contracts-app-500x500-pointer.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne300w/2020-0527-contracts-app-500x500-pointer.jpg 300w">
<div class="brief-description">
<h3 class="brief-title">Coronavirus Contracts: Tracking Federal Purchases to Fight the Pandemic</h3>
</div><!-- /end .brief-description -->
</img></a>
</div><!-- /end .promo-in-brief -->
<div class="promo-in-brief">
<a class="brief" href="//projects.propub3r6espa33w.onion/coronavirus/bailouts/">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/20200707-ppp-look-up-app-1x1-b.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne75w/20200707-ppp-look-up-app-1x1-b.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/20200707-ppp-look-up-app-1x1-b.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne300w/20200707-ppp-look-up-app-1x1-b.jpg 300w"/>
<div class="brief-description">
<h3 class="brief-title">Tracking PPP Loans: Search Every Company Approved for Federal Loans Over $150k</h3>
</div><!-- /end .brief-description -->
</a>
</div><!-- /end .promo-in-brief -->
<div class="promo-in-brief">
<a class="brief" href="//projects.propub3r6espa33w.onion/coronavirus-unemployment">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/20200720-unemployment-1x1-b.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne75w/20200720-unemployment-1x1-b.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/20200720-unemployment-1x1-b.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne300w/20200720-unemployment-1x1-b.jpg 300w"/>
<div class="brief-description">
<h3 class="brief-title">What Coronavirus Job Losses Reveal About Racism in America</h3>
</div><!-- /end .brief-description -->
</a>
</div><!-- /end .promo-in-brief -->
</div><!-- /end .covid-resources -->
</div><!-- /end .module-content -->
</div><!-- /end .pp-module -->
</div>
<div class="evergreen" id="level1b">
<div class="pp-module" id="featured-series-module">
<div class="module-content" id="featured-series">
<h2 class="pp-module-title">The Effort to Overturn the Election</h2>
<a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/series/the-insurrection">
<img alt="" class="series-img" sizes="100vw" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%2730%27%20height%3D%2740%27%20style%3D%27background%3Atransparent%27%2F%3E" srcset="https://img.assets-d.propublica.org/v5/images/20210113-insurrection-promo.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=1067&amp;q=80&amp;w=800&amp;s=109bd88547266597a32ae820aa2bea1c 800w, https://img.assets-d.propublica.org/v5/images/20210113-insurrection-promo.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=800&amp;q=75&amp;w=600&amp;s=e530f4df25f59078121b2d381582ec04 600w, https://img.assets-d.propublica.org/v5/images/20210113-insurrection-promo.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=400&amp;q=65&amp;w=300&amp;s=1c820505675eb71cd5060c3f0e3d1a2b 300w"/>
</a>
<div class="series-detail">
<p>Reporting on the mob that attacked and breached the Capitol, the fallout from that day, and ongoing far-right violence.</p>
</div>
</div>
</div>
<div class="pp-module promo-in-brief" id="featured-reporting-module">
<div class="module-content" id="featured-reporting">
<h2 class="pp-module-title">Featured Reporting</h2>
<a class="brief has-photo" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/solarwinds-cybersecurity-system">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210202-solarwinds-hack-in-toto_500x500.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210202-solarwinds-hack-in-toto_500x500.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210202-solarwinds-hack-in-toto_500x500.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210202-solarwinds-hack-in-toto_500x500.jpg 300w">
<div class="brief-description" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/solarwinds-cybersecurity-system">
<h3 class="brief-title">The U.S. Spent $2.2 Million on a Cybersecurity System That Wasn’t Implemented — and Might Have Stopped a Major Hack</h3>
</div><!-- /end .brief-description -->
</img></a>
<a class="brief has-photo" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/new-york-city-council-proposes-sweeping-nypd-reforms">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210201-nypd-reform-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210201-nypd-reform-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210201-nypd-reform-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210201-nypd-reform-1x1.jpg 300w"/>
<div class="brief-description" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/new-york-city-council-proposes-sweeping-nypd-reforms">
<h3 class="brief-title">New York City Council Proposes Sweeping NYPD Reforms</h3>
</div><!-- /end .brief-description -->
</a>
<a class="brief has-photo" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/boogaloo-bois-military-training">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210201-boog-military-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210201-boog-military-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210201-boog-military-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210201-boog-military-1x1.jpg 300w"/>
<div class="brief-description" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/boogaloo-bois-military-training">
<h3 class="brief-title">The Boogaloo Bois Have Guns, Criminal Records and Military Training. Now They Want to Overthrow the Government.</h3>
</div><!-- /end .brief-description -->
</a>
</div><!-- /end .module-content -->
</div>
</div>
<div class="river" id="level2">
<div class="story-entry region-national section-atPropublica">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/atpropublica/richard-tofel-to-retire-as-propublica-president-board-launches-search-for-successor">Richard Tofel to Retire as ProPublica President; Board Launches Search for Successor</a></h2>
<div class="metadata">
<p class="byline"><a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-17EST12:00">Feb. 17, 12 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-atPropublica">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/atpropublica/propublicas-nypd-files-wins-john-jay-college-harry-frank-guggenheim-award-for-excellence-in-criminal-justice-journalism">ProPublica’s “NYPD Files” Wins John Jay College/Harry Frank Guggenheim Award for Excellence in Criminal Justice Reporting</a></h2>
<div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-11EST15:16">Feb. 11, 3:16 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/fauci-vaccines-kids">
<img alt="" height="75" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%271%27%20height%3D%271%27%20style%3D%27background%3Atransparent%27%2F%3E" srcset="https://img.assets-d.propublica.org/v5/images/20210211-children-vaccine-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4764&amp;fp-y=0.4096&amp;h=75&amp;q=70&amp;w=75&amp;s=9c999c5d4aa6d85788d6b6cfdbbbbc6c 75w, https://img.assets-d.propublica.org/v5/images/20210211-children-vaccine-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4764&amp;fp-y=0.4096&amp;h=150&amp;q=70&amp;w=150&amp;s=5d603c0dca97f93efc19760d02409543 150w, https://img.assets-d.propublica.org/v5/images/20210211-children-vaccine-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4764&amp;fp-y=0.4096&amp;h=400&amp;q=70&amp;w=400&amp;s=b825ec6b6eaac28303a30fc6f1cf9ee5 400w" width="75"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/fauci-vaccines-kids">Fauci: Vaccines for Kids as Young as First Graders Could Be Authorized by September</a></h2>
<div class="dek"><p>For this to happen by the start of the next school year, trials need to prove the vaccine is safe and effective in children. Experts say manufacturers aren’t moving quickly enough.</p>
</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/caroline-chen">Caroline Chen</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-11EST05:00">Feb. 11, 5 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/utility-companies-owe-millions-to-this-state-regulatory-agency-the-problem-the-agency-cant-track-what-its-owed">
<img alt="" height="75" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%271%27%20height%3D%271%27%20style%3D%27background%3Atransparent%27%2F%3E" srcset="https://img.assets-d.propublica.org/v5/images/20210210-cpuc-folo-money-stebbins-hands.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4661&amp;fp-y=0.4299&amp;h=75&amp;q=70&amp;w=75&amp;s=f42a1e0ab6e2d363060816282d056160 75w, https://img.assets-d.propublica.org/v5/images/20210210-cpuc-folo-money-stebbins-hands.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4661&amp;fp-y=0.4299&amp;h=150&amp;q=70&amp;w=150&amp;s=143ebb34b7754aaa73a31bbc8b00840e 150w, https://img.assets-d.propublica.org/v5/images/20210210-cpuc-folo-money-stebbins-hands.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4661&amp;fp-y=0.4299&amp;h=400&amp;q=70&amp;w=400&amp;s=44e8f3f280110f546d7f4fc199ba2ef8 400w" width="75"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/utility-companies-owe-millions-to-this-state-regulatory-agency-the-problem-the-agency-cant-track-what-its-owed">Utility Companies Owe Millions to This State Regulatory Agency. The Problem? The Agency Can’t Track What It’s Owed.</a></h2>
<div class="dek">When a whistleblower alleged that $200 million was missing from the California Public Utilities Commission, the agency says it took steps to collect. Yet an audit uncovered more missing money and cited flaws in the agency’s accounting system.</div> <div class="metadata">
<p class="byline">by <span class="name">Scott Morris</span>,  <a href="http://www.baycitynews.org/">Bay City News Foundation</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-10EST14:00">Feb. 10, 2 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-atPropublica">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/atpropublica/megan-omatz-joins-propublicas-midwest-newsroom">Megan O’Matz Joins ProPublica’s Midwest Newsroom</a></h2>
<div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-10EST12:30">Feb. 10, 12:30 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/nsu-section-230">
<img alt="" height="75" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%271%27%20height%3D%271%27%20style%3D%27background%3Atransparent%27%2F%3E" srcset="https://img.assets-d.propublica.org/v5/images/20210209-nsu-section-230-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=75&amp;q=70&amp;w=75&amp;s=a7355a4865304a8029a4c14a5a6446aa 75w, https://img.assets-d.propublica.org/v5/images/20210209-nsu-section-230-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=150&amp;q=70&amp;w=150&amp;s=37cf6f9f8602eada0044fd112cf76b3b 150w, https://img.assets-d.propublica.org/v5/images/20210209-nsu-section-230-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=400&amp;q=70&amp;w=400&amp;s=310db174fef3d8a06adb7549229248e2 400w" width="75"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/nsu-section-230">Twenty-Six Words Created the Internet. What Will It Take to Save It?</a></h2>
<div class="dek">Jeff Kosseff wrote the book on Section 230, the law that gave us the internet we have today. He talks with ProPublica Editor-in-Chief Stephen Engelberg about how we got here and how we should regulate our way out.</div> <div class="metadata">
<p class="byline"><a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/stephen-engelberg">Stephen Engelberg</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-09EST14:00">Feb. 9, 2 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-atPropublica">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/atpropublica/two-propublica-local-reporting-network-projects-named-finalists-for-shadid-award-for-journalism-ethics">Two ProPublica Local Reporting Network Projects Named Finalists for Shadid Award for Journalism Ethics</a></h2>
<div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-08EST15:29">Feb. 8, 3:29 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-local-reporting-network section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/how-we-found-pricey-provisions-in-new-jersey-police-contracts">
<img alt="" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210208-nj-contracts-methodology-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210208-nj-contracts-methodology-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210208-nj-contracts-methodology-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210208-nj-contracts-methodology-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/how-we-found-pricey-provisions-in-new-jersey-police-contracts">How We Found Pricey Provisions in New Jersey Police Contracts</a></h2>
<div class="dek">ProPublica and the Asbury Park Press scoured hundreds of police union agreements for details on publicly funded payouts to cops.</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/agnes-chang">Agnes Chang</a>, <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/jeff-kao">Jeff Kao</a> and <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/agnel-philip">Agnel Philip</a>, ProPublica, and <span class="name">Andrew Ford</span>,  <a href="https://www.app.com/">Asbury Park Press</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-08EST05:01">Feb. 8, 5:01 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-local-reporting-network section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/new-jersey-police-contracts">
<img alt="" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210208-new-jersey-union-contracts-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210208-new-jersey-union-contracts-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210208-new-jersey-union-contracts-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210208-new-jersey-union-contracts-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/new-jersey-police-contracts">How the Police Bank Millions Through Their Union Contracts</a></h2>
<div class="dek">The public funds six-figure “sick day” payouts, $2,500 “perfect attendance” bonuses and lucrative “extra duty” assignments identified in a ProPublica, Asbury Park Press analysis of New Jersey police union contracts.</div> <div class="metadata">
<p class="byline">by <span class="name">Andrew Ford</span>,  <a href="https://www.app.com/">Asbury Park Press</a>, and <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/agnes-chang">Agnes Chang</a>, <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/jeff-kao">Jeff Kao</a> and <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/agnel-philip">Agnel Philip</a>, ProPublica</p> <p class="timestamp"><time class="timestamp" datetime="2021-02-08EST05:00">Feb. 8, 5 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-local-reporting-network section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/hawaii-beaches-legislation">
<img alt="" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210206-hawaii-sandbags-shore-impact-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210206-hawaii-sandbags-shore-impact-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210206-hawaii-sandbags-shore-impact-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210206-hawaii-sandbags-shore-impact-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/hawaii-beaches-legislation">Hawaii’s Beaches Are Disappearing. New Legislation Could Help ... if It’s Enforced.</a></h2>
<div class="dek">A legal loophole allowed wealthy property owners to protect their real estate at the expense of Hawaii’s coastlines. Now, the state Legislature is considering bills to crack down on the destructive practices, but questions around enforcement remain.</div> <div class="metadata">
<p class="byline">by <span class="name">Sophie Cocke</span>,  <a href="https://www.staradvertiser.com/">Honolulu Star-Advertiser</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-07EST05:01">Feb. 7, 5:01 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/why-opening-restaurants-is-exactly-what-the-coronavirus-wants-us-to-do">
<img alt="" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210206-variants-reopening-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210206-variants-reopening-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210206-variants-reopening-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210206-variants-reopening-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/why-opening-restaurants-is-exactly-what-the-coronavirus-wants-us-to-do">Why Opening Restaurants Is Exactly What the Coronavirus Wants Us to Do</a></h2>
<div class="dek">Governors continue to open indoor dining and other activities before vaccinations become widespread. Experts warn this could create superspreading playgrounds for dangerous variants and squander our best shot at getting the pandemic under control.</div> <div class="metadata">
<p class="byline"><a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/caroline-chen">Caroline Chen</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-06EST05:00">Feb. 6, 5 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-local-reporting-network section-articles">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/propublica-is-seeking-new-applicants-for-its-local-reporting-network">ProPublica Is Seeking New Applicants for Its Local Reporting Network</a></h2>
<div class="dek">We are looking to work with three more newsrooms, for a year beginning in April 2021, on accountability journalism projects.</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-05EST11:07">Feb. 5, 11:07 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-atPropublica">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/atpropublica/propublica-hires-jenny-deam-to-cover-health-care">ProPublica Hires Jenny Deam to Cover Health Care</a></h2>
<div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-05EST11:00">Feb. 5, 11 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/we-have-counties-in-deep-trouble-oregon-lawmakers-seek-to-reverse-timber-tax-cuts-that-cost-communities-billions">
<img alt="" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210205-oregon-timber-impact-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210205-oregon-timber-impact-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210205-oregon-timber-impact-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210205-oregon-timber-impact-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/we-have-counties-in-deep-trouble-oregon-lawmakers-seek-to-reverse-timber-tax-cuts-that-cost-communities-billions">“We Have Counties in Deep Trouble”: Oregon Lawmakers Seek to Reverse Timber Tax Cuts That Cost Communities Billions</a></h2>
<div class="dek">For decades, corporate timber benefited from tax cuts that devastated local budgets. Lawmakers want change and have filed dozens of bills, making this one of Oregon’s most consequential sessions for forest policy.</div> <div class="metadata">
<p class="byline">by <span class="name">Rob Davis</span>,  <a href="https://www.oregonlive.com/">The Oregonian/OregonLive</a>, and <span class="name">Tony Schick</span>,  Oregon Public Broadcasting</p> <p class="timestamp"><time class="timestamp" datetime="2021-02-05EST09:00">Feb. 5, 9 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/rich-investors-stripped-millions-from-a-hospital-chain-and-want-to-leave-it-behind-a-tiny-state-stands-in-their-way">
<img alt="An illustration of a hand taking apart a hospital." src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210204-private-equity-illo-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210204-private-equity-illo-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210204-private-equity-illo-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210204-private-equity-illo-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/rich-investors-stripped-millions-from-a-hospital-chain-and-want-to-leave-it-behind-a-tiny-state-stands-in-their-way">Rich Investors Stripped Millions From a Hospital Chain and Want to Leave It Behind. A Tiny State Stands in Their Way.</a></h2>
<div class="dek">Private equity firm Leonard Green and other investors extracted $645 million from Prospect Medical before announcing a deal to sell it and leave it with $1.3 billion in financial obligations. Four states approved it — but Rhode Island is holding out.</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/peter-elkind">Peter Elkind</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-04EST13:22">Feb. 4, 1:22 p.m. EST</time>
</p>
</div>
</div>
</div>
<div id="content-bottom"></div>
<footer class="browse-by-month">
<form>
<nav>
<label for="browse-archive">Browse the archive:</label>
<select class="menu-jump" id="browse-archive" name="browse-archive">
<option value="">Choose a month</option>
<option value="/archive/2021/02">February 2021</option>
<option value="/archive/2021/01">January 2021</option>
<option value="/archive/2020/12">December 2020</option>
<option value="/archive/2020/11">November 2020</option>
<option value="/archive/2020/10">October 2020</option>
<option value="/archive/2020/09">September 2020</option>
<option value="/archive/2020/08">August 2020</option>
<option value="/archive/2020/07">July 2020</option>
<option value="/archive/2020/06">June 2020</option>
<option value="/archive/2020/05">May 2020</option>
<option value="/archive/2020/04">April 2020</option>
<option value="/archive/2020/03">March 2020</option>
<option value="/archive/2020/02">February 2020</option>
<option value="/archive/2020/01">January 2020</option>
<option value="/archive/2019/12">December 2019</option>
<option value="/archive/2019/11">November 2019</option>
<option value="/archive/2019/10">October 2019</option>
<option value="/archive/2019/09">September 2019</option>
<option value="/archive/2019/08">August 2019</option>
<option value="/archive/2019/07">July 2019</option>
<option value="/archive/2019/06">June 2019</option>
<option value="/archive/2019/05">May 2019</option>
<option value="/archive/2019/04">April 2019</option>
<option value="/archive/2019/03">March 2019</option>
<option value="/archive/2019/02">February 2019</option>
<option value="/archive/2019/01">January 2019</option>
<option value="/archive/2018/12">December 2018</option>
<option value="/archive/2018/11">November 2018</option>
<option value="/archive/2018/10">October 2018</option>
<option value="/archive/2018/09">September 2018</option>
<option value="/archive/2018/08">August 2018</option>
<option value="/archive/2018/07">July 2018</option>
<option value="/archive/2018/06">June 2018</option>
<option value="/archive/2018/05">May 2018</option>
<option value="/archive/2018/04">April 2018</option>
<option value="/archive/2018/03">March 2018</option>
<option value="/archive/2018/02">February 2018</option>
<option value="/archive/2018/01">January 2018</option>
<option value="/archive/2017/12">December 2017</option>
<option value="/archive/2017/11">November 2017</option>
<option value="/archive/2017/10">October 2017</option>
<option value="/archive/2017/09">September 2017</option>
<option value="/archive/2017/08">August 2017</option>
<option value="/archive/2017/07">July 2017</option>
<option value="/archive/2017/06">June 2017</option>
<option value="/archive/2017/05">May 2017</option>
<option value="/archive/2017/04">April 2017</option>
<option value="/archive/2017/03">March 2017</option>
<option value="/archive/2017/02">February 2017</option>
<option value="/archive/2017/01">January 2017</option>
<option value="/archive/2016/12">December 2016</option>
<option value="/archive/2016/11">November 2016</option>
<option value="/archive/2016/10">October 2016</option>
<option value="/archive/2016/09">September 2016</option>
<option value="/archive/2016/08">August 2016</option>
<option value="/archive/2016/07">July 2016</option>
<option value="/archive/2016/06">June 2016</option>
<option value="/archive/2016/05">May 2016</option>
<option value="/archive/2016/04">April 2016</option>
<option value="/archive/2016/03">March 2016</option>
<option value="/archive/2016/02">February 2016</option>
<option value="/archive/2016/01">January 2016</option>
<option value="/archive/2015/12">December 2015</option>
<option value="/archive/2015/11">November 2015</option>
<option value="/archive/2015/10">October 2015</option>
<option value="/archive/2015/09">September 2015</option>
<option value="/archive/2015/08">August 2015</option>
<option value="/archive/2015/07">July 2015</option>
<option value="/archive/2015/06">June 2015</option>
<option value="/archive/2015/05">May 2015</option>
<option value="/archive/2015/04">April 2015</option>
<option value="/archive/2015/03">March 2015</option>
<option value="/archive/2015/02">February 2015</option>
<option value="/archive/2015/01">January 2015</option>
<option value="/archive/2014/12">December 2014</option>
<option value="/archive/2014/11">November 2014</option>
<option value="/archive/2014/10">October 2014</option>
<option value="/archive/2014/09">September 2014</option>
<option value="/archive/2014/08">August 2014</option>
<option value="/archive/2014/07">July 2014</option>
<option value="/archive/2014/06">June 2014</option>
<option value="/archive/2014/05">May 2014</option>
<option value="/archive/2014/04">April 2014</option>
<option value="/archive/2014/03">March 2014</option>
<option value="/archive/2014/02">February 2014</option>
<option value="/archive/2014/01">January 2014</option>
<option value="/archive/2013/12">December 2013</option>
<option value="/archive/2013/11">November 2013</option>
<option value="/archive/2013/10">October 2013</option>
<option value="/archive/2013/09">September 2013</option>
<option value="/archive/2013/08">August 2013</option>
<option value="/archive/2013/07">July 2013</option>
<option value="/archive/2013/06">June 2013</option>
<option value="/archive/2013/05">May 2013</option>
<option value="/archive/2013/04">April 2013</option>
<option value="/archive/2013/03">March 2013</option>
<option value="/archive/2013/02">February 2013</option>
<option value="/archive/2013/01">January 2013</option>
<option value="/archive/2012/12">December 2012</option>
<option value="/archive/2012/11">November 2012</option>
<option value="/archive/2012/10">October 2012</option>
<option value="/archive/2012/09">September 2012</option>
<option value="/archive/2012/08">August 2012</option>
<option value="/archive/2012/07">July 2012</option>
<option value="/archive/2012/06">June 2012</option>
<option value="/archive/2012/05">May 2012</option>
<option value="/archive/2012/04">April 2012</option>
<option value="/archive/2012/03">March 2012</option>
<option value="/archive/2012/02">February 2012</option>
<option value="/archive/2012/01">January 2012</option>
<option value="/archive/2011/12">December 2011</option>
<option value="/archive/2011/11">November 2011</option>
<option value="/archive/2011/10">October 2011</option>
<option value="/archive/2011/09">September 2011</option>
<option value="/archive/2011/08">August 2011</option>
<option value="/archive/2011/07">July 2011</option>
<option value="/archive/2011/06">June 2011</option>
<option value="/archive/2011/05">May 2011</option>
<option value="/archive/2011/04">April 2011</option>
<option value="/archive/2011/03">March 2011</option>
<option value="/archive/2011/02">February 2011</option>
<option value="/archive/2011/01">January 2011</option>
<option value="/archive/2010/12">December 2010</option>
<option value="/archive/2010/11">November 2010</option>
<option value="/archive/2010/10">October 2010</option>
<option value="/archive/2010/09">September 2010</option>
<option value="/archive/2010/08">August 2010</option>
<option value="/archive/2010/07">July 2010</option>
<option value="/archive/2010/06">June 2010</option>
<option value="/archive/2010/05">May 2010</option>
<option value="/archive/2010/04">April 2010</option>
<option value="/archive/2010/03">March 2010</option>
<option value="/archive/2010/02">February 2010</option>
<option value="/archive/2010/01">January 2010</option>
<option value="/archive/2009/12">December 2009</option>
<option value="/archive/2009/11">November 2009</option>
<option value="/archive/2009/10">October 2009</option>
<option value="/archive/2009/09">September 2009</option>
<option value="/archive/2009/08">August 2009</option>
<option value="/archive/2009/07">July 2009</option>
<option value="/archive/2009/06">June 2009</option>
<option value="/archive/2009/05">May 2009</option>
<option value="/archive/2009/04">April 2009</option>
<option value="/archive/2009/03">March 2009</option>
<option value="/archive/2009/02">February 2009</option>
<option value="/archive/2009/01">January 2009</option>
<option value="/archive/2008/12">December 2008</option>
<option value="/archive/2008/11">November 2008</option>
<option value="/archive/2008/10">October 2008</option>
<option value="/archive/2008/09">September 2008</option>
<option value="/archive/2008/08">August 2008</option>
<option value="/archive/2008/07">July 2008</option>
<option value="/archive/2008/06">June 2008</option>
<option value="/archive/2008/05">May 2008</option>
<option value="/archive/2008/04">April 2008</option>
<option value="/archive/2008/03">March 2008</option>
<option value="/archive/2008/02">February 2008</option>
<option value="/archive/2008/01">January 2008</option>
<option value="/archive/2007/12">December 2007</option>
<option value="/archive/2007/11">November 2007</option>
<option value="/archive/2007/10">October 2007</option>
</select>
</nav></form>
</footer></div>

<!-- end #level2.river -->
<div class="promos-1" id="level3">
<div class="pp-module module-follow" id="l1">
<h2 class="pp-module-title">Follow ProPublica</h2>
<ul class="list-plain links-social">
<li>
<a class="btn btn-social social-main social-link-twitter" href="https://twitter.com/propublica" id="twitter-follow-pp-module">
<svg class="icon" height="24" width="24"><use xlink:href="#icon-twitter"></use></svg>
<span>Twitter</span>
</a>
</li>
<li>
<a class="btn btn-social social-main social-link-fb" href="https://www.facebookcorewwwi.onion/propublica/" id="facebook-follow-pp-module">
<svg class="icon" height="24" width="24"><use xlink:href="#icon-facebook"></use></svg>
<span>Facebook</span>
</a>
</li>
<li>
<a class="btn btn-social social-main social-link-youtube" href="https://www.youtube.com/user/propublica" id="youtube-follow-pp-module">
<svg class="icon" height="24" width="24"><use xlink:href="#icon-youtube"></use></svg>
<span>YouTube</span>
</a>
</li>
<li>
<a class="btn btn-social social-main social-link-rss" href="//feeds.propub3r6espa33w.onion/propublica/main" id="rss-follow-pp-module">
<svg class="icon" height="24" width="24"><use xlink:href="#icon-rss"></use></svg>
<span><abbr title="Rich Site Summary">RSS</abbr></span>
</a>
</li>
</ul>
<form action="https://signup.propublica.org/newsletter/turing" class="ajax-form form-subscribe" id="sidebar-email-subscribe-module" method="post">
<h2 class="pp-module-title">Stay Informed</h2>
<p class="pp-module-description">Get our investigations delivered to your inbox with the Big Story newsletter.</p>
<div class="form-label-set">
<input class="input-text" id="sidebar-email-subscribe-input" name="email" onfocus="recaptchaInit()" placeholder="Enter your email" required="" type="email"/>
<label for="sidebar-email-subscribe-input">
<span class="a11y">Email address</span>
<svg aria-hidden="true" class="icon"><use xlink:href="#icon-email"></use></svg>
</label>
</div>
<input class="g-recaptcha" id="newsletter-signup-follow-main" type="submit" value="Sign Up"/>
<input name="success_location" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/forms/thankyou"/>
<input name="error_location" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/forms/error"/>
<input name="target" type="hidden" value="l/125411/2018-11-01/5vd2q2"/>
<input name="origin_location" type="hidden" value="https://v3-www.propublica.org/"/>
<small class="legal">This site is protected by reCAPTCHA and the Google <a href="https://policies.google.com/privacy">Privacy Policy</a> and <a href="https://policies.google.com/terms">Terms of Service</a> apply.</small>
</form>
</div><!-- end .pp-module.module-follow -->
<a class="pp-module module-donate has-photo" href="https://donate.propublica.org/" id="donate-module">
<div class="module-photo is-bg" style="background-image: url('//assets.propub3r6espa33w.onion/static/prod/v4/images/donate-photo-a.png');"></div>
<div class="module-content donate-content">
<h2 class="module-hed hed-donate">It’s not too late to <strong>Vote ProPublica</strong></h2>
<span class="btn btn-urgent">Donate</span>
</div><!-- /end .module-content -->
</a><!-- /end .pp-module.module-donate.has-bg -->
<div class="pp-module module-awards has-photo" id="l3">
<p class="module-photo">
<img alt="" src="//assets.propub3r6espa33w.onion/static/prod/v4/images/medal_franklin_128.png"/>
</p>
<div class="module-content">
<h2 class="pp-module-title">Awards</h2>
<p>ProPublica has been a recipient of the Pulitzer Prizes for public service, explanatory reporting, national reporting, investigative reporting and feature writing. <a href="/awards" id="awards-module">See the full list of our awards</a>.</p>
</div><!-- end .module-content -->
</div><!-- end .pp-module.module-awards -->
<div class="pp-module module-corrections" id="l4">
<div class="module-content">
<h2 class="pp-module-title">Complaints &amp; Corrections</h2>
<p>To contact us with concerns and corrections, <a href="/cdn-cgi/l/email-protection#5b3d3e3e3f393a38301b2b29342b2e393732383a7534293c">email us</a>. All emails may be published unless you tell us otherwise. <a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/corrections" id="corrections-module">Read our corrections</a>.</p>
</div><!-- end .module-content -->
</div><!-- end .pp-module.module-corrections -->
</div><!-- end #level3.promos-1 -->
<div class="promos-2" id="level4">
<!-- /2219821/Desktop_MedRec_1 -->
<div class="ad ad-article ad-300x250">
<div class="inner htl-ad" data-eager="" data-prebid="0x0:|960x0:Desktop_Medrec_1" data-refresh="viewable" data-refresh-secs="60" data-sizes="0x0:|960x0:1x1,300x250,300x600" data-unit="Desktop_Medrec_1"></div>
</div><!-- end .ad -->
<div class="site-promo-module" id="r3">
<a href="/nerds" id="nerd-blog-promo">
<img alt="" src="//assets.propub3r6espa33w.onion/static/prod/v4/images/promo-nerd-blog.gif"/>
<div class="site-text">
<h2 class="site-title">ProPublica Nerd Blog</h2>
<p class="site-description">Secrets for data journalists and newsroom developers
                        </p>
</div>
</a>
</div>
<div class="site-promo-module" id="r4">
<a href="/datastore" id="datastore-promo">
<img alt="" src="//assets.propub3r6espa33w.onion/static/prod/v4/images/promo-data-store.gif"/>
<div class="site-text">
<h2 class="site-title">ProPublica Data Store</h2>
<p class="site-description">Download or purchase the data behind our journalism</p>
</div>
</a>
</div>
<div class="pp-module promo-in-brief" id="r5">
<div class="module-content">
<a class="brief has-photo" href="/series/trump-inc" id="podcast-promo">
<img alt="" class="brief-thumb" src="//assets.propub3r6espa33w.onion/static/prod/v4/images/promo-podcast-trump-inc.jpg"/>
<div class="brief-description">
<h2 class="hed">Podcast: Trump, Inc.</h2>
<p class="brief-title">Exploring the mysteries of the president’s businesses, who profits and at what cost.</p>
</div>
</a>
</div>
</div>
<!-- end .pp-module.promo-in-brief -->
<div class="pp-module promo-in-brief" id="r6">
<div class="module-content" id="get-involved-module">
<h2 class="pp-module-title">Get Involved</h2>
<a class="brief" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/getinvolved/juvenile-justice-fees">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/2021-0128-juvenile-justice-folo-500x500.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne75w/2021-0128-juvenile-justice-folo-500x500.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/2021-0128-juvenile-justice-folo-500x500.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne300w/2021-0128-juvenile-justice-folo-500x500.jpg 300w"/>
<div class="brief-description">
<h3 class="brief-title">Has Your Family Paid Fees or Fines to a Juvenile Justice System?</h3>
</div><!-- /end .brief-description -->
</a>
<a class="brief" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/getinvolved/help-propublica-find-the-most-important-stories-of-2021">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/20210113-pitch-propublica-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne75w/20210113-pitch-propublica-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/20210113-pitch-propublica-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne300w/20210113-pitch-propublica-1x1.jpg 300w"/>
<div class="brief-description">
<h3 class="brief-title">Help ProPublica Find the Most Important Stories of 2021</h3>
</div><!-- /end .brief-description -->
</a>
<a class="brief" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/getinvolved/did-you-attend-the-milton-hershey-school-were-investigating-it-help-us">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/20201123-hershey-callout-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne75w/20201123-hershey-callout-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/20201123-hershey-callout-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne300w/20201123-hershey-callout-1x1.jpg 300w"/>
<div class="brief-description">
<h3 class="brief-title">Did You Attend the Milton Hershey School? We’re Reporting on It. Help Us.</h3>
</div><!-- /end .brief-description -->
</a>
</div><!-- /end .module-content -->
</div><!-- /end .pp-module.promo-in-brief -->
<div class="pp-module module-popular" id="r7">
<h2 class="pp-module-title">Most Popular Stories</h2>
<div class="module-content" id="hottest-stories-module">
<div class="tabs">
<div class="pane-stories collapsible" data-collapsible-set="pp-stories-hottest">
<h3 class="hed-stories">Most Read</h3>
<ul class="list-plain list-stories">
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/dying-on-the-waitlist" title="View this">Dying on the Waitlist</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/i-dont-trust-the-people-above-me-riot-squad-cops-open-up-about-disastrous-response-to-capitol-insurrection" title="View this">“I Don’t Trust the People Above Me”: Riot Squad Cops Open Up About Disastrous Response to Capitol Insurrection</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/turbotax-just-tricked-you-into-paying-to-file-your-taxes" title="View this">Here’s How TurboTax Just Tricked You Into Paying to File Your Taxes</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/the-irs-cashed-her-check-then-the-late-notices-started-coming" title="View this">The IRS Cashed Her Check. Then the Late Notices Started Coming.</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/people-over-75-are-first-in-line-to-be-vaccinated-against-covid-19-the-average-black-person-doesnt-live-that-long" title="View this">People Over 75 Are First in Line to Be Vaccinated Against COVID-19. The Average Black Person Here Doesn’t Live That Long.</a></li>
</ul>
</div><!-- /end .pane-stories -->
<div class="pane-stories collapsible collapsible-collapsed" data-collapsible-set="pp-stories-hottest">
<h3 class="hed-stories">Most Emailed</h3>
<ul class="list-plain list-stories">
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/the-irs-cashed-her-check-then-the-late-notices-started-coming" title="View this">The IRS Cashed Her Check. Then the Late Notices Started Coming.</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/i-dont-trust-the-people-above-me-riot-squad-cops-open-up-about-disastrous-response-to-capitol-insurrection" title="View this">“I Don’t Trust the People Above Me”: Riot Squad Cops Open Up About Disastrous Response to Capitol Insurrection</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/turbotax-just-tricked-you-into-paying-to-file-your-taxes" title="View this">Here’s How TurboTax Just Tricked You Into Paying to File Your Taxes</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/dying-on-the-waitlist" title="View this">Dying on the Waitlist</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/a-federal-appeals-court-has-ruled-in-favor-of-releasing-nypd-discipline-records" title="View this">A Federal Appeals Court Has Ruled in Favor of Releasing NYPD Discipline Records</a></li>
</ul>
</div><!-- /end .pane-stories -->
</div><!-- /end .tabs -->
</div><!-- /end .module-content -->
</div><!-- /end .pp-module.module-popular -->
<!-- /2219821/Desktop_MedRec_2 -->
<div class="ad ad-article ad-300x250">
<div class="inner htl-ad" data-prebid="0x0:|960x0:Desktop_Medrec_2" data-refresh="viewable" data-refresh-secs="60" data-sizes="0x0:|960x0:1x1,300x250" data-unit="Desktop_Medrec_2"></div>
</div><!-- end .ad -->
</div><!-- end #level4.promos-2 -->
<!-- end .wrapper -->
<!-- end .content -->
<div class="modal collapsible collapsible-collapsed" id="modal-search">
<div class="collapsible-header close-btn">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-close-light"></use>
</svg>
<span class="a11y">Close this screen</span>
</div>
<!-- end .collapsible-header -->
<div class="collapsible-content content">
<form action="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/search" class="nav-site-search" method="get">
<label class="a11y hed-form hed-site-search">Search ProPublica:</label>
<div class="input-wrapper reverse">
<input class="text nav-site-search-text" name="qss" placeholder="Search ProPublica" type="search" value=""/>
<input type="submit" value="Search"/>
</div>
</form>
</div>
<!-- end .collapsible-content -->
</div>
<!-- end #modal-search -->
<div class="modal collapsible collapsible-collapsed" id="modal-republish">
<div class="collapsible-header close-btn">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-close-light"></use>
</svg>
<span class="a11y">Close this screen</span>
</div>
<!-- end .collapsible-header -->
<div class="collapsible-content content">
<div class="wrapper">
<h3>Republish This Story for Free</h3>
<p class="license"><a href="https://creativecommons.org/licenses/by-nc-nd/3.0/" rel="license">Creative Commons License (CC BY-NC-ND 3.0)</a></p>
<div class="republish-instructions">
<h3 class="title"></h3>
<p class="byline"></p>
<p class="intro">Thank you for your interest in republishing this story. You are are free to republish it so long as you do the following:</p>
<ul>
<li>You have to credit us. In the byline, we prefer “Author Name, ProPublica.” At the top of the text of your story, include a line that reads: “This story was originally published by ProPublica.” You must link the word “ProPublica” to the original URL of the story.</li>
<li>If you’re republishing online, you must link to the URL of this story on propublica.org, include all of the links from our story, including our newsletter sign up language and link, and use our <a href="/pixelping">PixelPing tag</a>.</li>
<li>If you use canonical metadata, please use the ProPublica URL. For more information about canonical metadata, <a href="https://support.google.com/webmasters/answer/139066?hl=en">refer to this Google SEO link</a>.</li>
<li>You can’t edit our material, except to reflect relative changes in time, location and editorial style. (For example, “yesterday” can be changed to “last week,” and “Portland, Ore.” to “Portland” or “here.”)</li>
<li>You cannot republish our photographs or illustrations without specific permission. Please contact <a href="/cdn-cgi/l/email-protection#d598b0b1bcb487bcb2bda1a695a5a7baa5a0b7b9bcb6b4fbbaa7b2"><span class="__cf_email__" data-cfemail="d39eb6b7bab281bab4bba7a093a3a1bca3a6b1bfbab0b2fdbca1b4">[email protected]</span></a>.</li>
<li>It’s okay to put our stories on pages with ads, but not ads specifically sold against our stories. You can’t state or imply that donations to your organization support ProPublica’s work.</li>
<li>You can’t sell our material separately or syndicate it. This includes publishing or syndicating our work on platforms or apps such as Apple News, Google News, etc.</li>
<li>You can’t republish our material wholesale, or automatically; you need to select stories to be republished individually. (To inquire about syndication or licensing opportunities, contact our Vice President of Business Development, <a href="/people/celeste-lecompte">Celeste LeCompte</a>.)</li>
<li>You can’t use our work to populate a website designed to improve rankings on search engines or solely to gain revenue from network-based advertisements.</li>
<li>We do not generally permit translation of our stories into another language.</li>
<li>Any website our stories appear on must include a prominent and effective way to contact you.</li>
<li>If you share republished stories on social media, we’d appreciate being tagged in your posts. We have official accounts for ProPublica and ProPublica Illinois on both Twitter (<a href="https://twitter.com/propublica">@ProPublica</a> and <a href="https://twitter.com/propublicail">@ProPublicaIL</a>) and Facebook.</li>
</ul>
</div>
<!-- end .republish-content -->
<div class="republish-copy">
<p>Copy and paste the following into your page to republish:</p>
</div>
<!-- end .republish-copy -->
</div>
<!-- end .wrapper -->
</div>
<!-- end .collapsible-content -->
</div>
<!-- end #modal-republish -->
<div class="modal collapsible collapsible-collapsed" id="nav-menu">
<div class="collapsible-header close-btn">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-close-light"></use>
</svg>
<span class="a11y">Close this menu</span>
</div>
<div class="collapsible-content">
<nav class="primary-nav">
<ul>
<li class="primary-nav-item news-apps"><a href="/newsapps/">Graphics &amp; Data</a></li>
<li class="primary-nav-item topics"><a href="/topics/">Topics</a></li>
<li class="primary-nav-item series"><a href="/series/">Series</a></li>
<li class="primary-nav-item videos"><a href="/video/">Videos</a></li>
<li class="primary-nav-item impact"><a href="/impact/">Impact</a></li>
</ul>
</nav><!-- end .primary-nav -->
<nav class="org-nav">
<ul>
<li class="org-nav-item main"><a href="/">ProPublica</a></li>
<li class="org-nav-item local-initiatives"><a href="/local-initiatives/">Local Initiatives</a></li>
<li class="org-nav-item data-store"><a href="/datastore/">Data Store</a></li>
</ul>
</nav><!-- end .org-nav -->
<nav class="action-nav">
<p>Follow Us:</p>
<ul>
<li class="action-nav-item social facebook">
<a href="https://www.facebookcorewwwi.onion/propublica">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-facebook"></use>
</svg>
<span class="cta">Like us on Facebook</span>
</a>
</li>
<li class="action-nav-item social twitter">
<a href="https://twitter.com/propublica">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-twitter"></use>
</svg>
<span class="cta">Follow us on Twitter</span>
</a>
</li>
</ul>
</nav><!-- end .action-nav -->
</div><!-- end .modal-inner -->
</div><!-- end .modal -->
<!-- jQuery/Legacy JS -->
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/js/public/assets/all.js"></script>
<script src="//assets.propub3r6espa33w.onion/static/prod/v4/js/main.3604a59f.js"></script>
<script src="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/js/public/assets/beacons.js"></script>
<script src="//connect.facebook.net/en_US/all.js"></script>
<div id="fb-root"></div>
<script>
            FB.init({
                appId: '229862657130557', // App ID
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true // parse XFBML
            });
    </script>
<!-- FOOTER -->
<footer role="contentinfo">
<div class="newsletter">
<div class="site-footer-inner">
<form action="https://signup.propublica.org/newsletter/turing" class="ajax-form subscribe-email" method="post">
<h2 class="pp-module-title" id="digest-title">Stay informed with the Daily Digest.</h2>
<div aria-describedby="digest-title" class="form-wrap" role="group">
<label class="a11y" for="pp-email-signup--full">Enter your email</label>
<input class="text subscribe-email-text" id="pp-email-signup--full" name="email" onfocus="recaptchaInit()" placeholder="Enter your email" type="email"/>
<input class="btn subscribe-email-btn g-recaptcha" id="newsletter-signup-footer" type="submit" value="Sign Up"/>
<input name="target" type="hidden" value="l/125411/2018-11-01/5vd35q"/>
<input name="stlist" type="hidden" value="The Daily Digest"/>
<input name="source" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/"/>
<input name="placement" type="hidden" value="site-wide-footer"/>
<input name="region" type="hidden" value=""/>
<input name="success_location" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/forms/thankyou"/>
<input name="error_location" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/forms/error"/>
<input name="origin_location" type="hidden" value="https://v3-www.propublica.org/"/>
</div>
</form><!-- end .subscribe-email -->
</div>
</div><!-- end .newsletter -->
<div class="site-footer-inner">
<nav aria-labelledby="footer-nav" id="footer-links">
<h2 class="a11y" id="footer-nav">Site Navigation</h2>
<div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>Sections</h3>
<ul>
<li><a href="/">ProPublica</a></li>
<li><a href="/local-reporting-network/">Local Reporting Network</a></li>
<li><a href="/texas/">Texas Tribune Partnership</a></li>
<li><a href="/datastore/">The Data Store</a></li>
<li><a href="/electionland/">Electionland</a></li>
</ul>
</div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>Browse by Type</h3>
<ul>
<li><a href="/topics/">Topics</a></li>
<li><a href="/series/">Series</a></li>
<li><a href="/video/">Videos</a></li>
<li><a href="/newsapps/">News Apps</a></li>
<li><a href="/getinvolved/">Get Involved</a></li>
<li><a href="/nerds/">The Nerd Blog</a></li>
<li><a href="/atpropublica/">@ProPublica</a></li>
<li><a href="/events/">Events</a></li>
</ul>
</div>
</div>
<div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>Info</h3>
<ul>
<li><a href="/about/">About Us</a></li>
<li><a href="/leadership/">Board and Advisors</a></li>
<li><a href="/staff/">Officers and Staff</a></li>
<li><a href="/diversity/">Diversity</a></li>
<li><a href="/jobs/">Jobs</a> and <a href="/fellowships/">Fellowships</a></li>
<li><a href="/local-initiatives/">Local Initiatives</a></li>
<li><a href="/media-center/">Media Center</a></li>
<li><a href="/reports/">Reports</a></li>
<li><a href="/impact/">Impact</a></li>
<li><a href="/awards/">Awards</a></li>
<li><a href="/corrections/">Corrections</a></li>
</ul>
</div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>Policies</h3>
<ul>
<li><a href="/code-of-ethics/">Code of Ethics</a></li>
<li><a href="/advertising/">Advertising Policy</a></li>
<li><a href="/legal/">Privacy Policy</a></li>
</ul>
</div>
</div>
<div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>Follow</h3>
<ul>
<li><a href="/newsletters/">Newsletters</a></li>
<li><a href="/series/trump-inc/">Podcast</a></li>
<li><a href="https://itunes.apple.com/us/app/propublica/id355298887?mt=8">iOS</a> and <a href="https://play.google.com/store/apps/details?id=com.propublica&amp;hl=en">Android</a></li>
<li><a href="//feeds.propub3r6espa33w.onion/propublica/main">RSS Feed</a></li>
</ul>
</div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>More</h3>
<ul>
<li><a href="/tips/">Send Us Tips</a></li>
<li><a href="/steal-our-stories/">Steal Our Stories</a></li>
<li><a href="https://www.propub3r6espa33w.onion">Browse via Tor</a></li>
<li><a href="/contact/">Contact Us</a></li>
<li><a href="https://donate.propublica.org/">Donate</a></li>
</ul>
</div>
</div>
</nav>
<div class="site-copyright">
<a class="logo" href="/">
<svg height="75" role="img" width="574">
<use xlink:href="#propublica-logo"></use>
<text class="a11y">ProPublica</text>
</svg>
</a>
<p class="slogan">Journalism in the Public Interest</p>
<small>© Copyright 2021 Pro Publica Inc.</small>
</div>
</div><!-- end .site-footer-inner -->
</footer>
<!-- anchors used by `aria-describedby` states -->
<div class="squelch" id="wayfinding">
<span id="current-site">Current site</span>
<span id="current-page">Current page</span>
</div>
<script data-delay="3" data-endpoint="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion//partials/newsletter-roadblock-big-story.html" data-stylesheet="//assets.propub3r6espa33w.onion/static/prod/v4/css/deploy/syndicated-newsletter.css" data-syndicate="" data-target="l/125411/2019-04-26/6m938v" src="//assets.propub3r6espa33w.onion/static/prod/v4/js/deploy/syndicated-newsletter-v1.1.0.js">
</script>
<!-- END FOOTER -->
<script src="https://www.google.com/recaptcha/api.js?onload=grecaptchaLoaded&amp;render=6LdI1rAUAAAAACI0GsFv-yRpC0tPF5ECiIMDUz2x"></script>
<script type="application/ld+json">{"@context":"http://schema.org","@graph":[{"@type":"WebPage","author":{"@id":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion#identity"},"copyrightHolder":{"@id":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion#identity"},"copyrightYear":"2019","creator":{"@id":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion#creator"},"dateModified":"2021-02-18T15:29:57-05:00","datePublished":"2019-10-31T13:02:00-04:00","description":"ProPublica is an independent, non-profit newsroom that produces investigative journalism in the public interest.","headline":"ProPublica — Investigative Journalism and News in the Public Interest","image":{"@type":"ImageObject","url":"//assets.propub3r6espa33w.onion/2017-pp-open-graph-1200x630.jpg"},"inLanguage":"en-us","mainEntityOfPage":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/","name":"ProPublica — Investigative Journalism and News in the Public Interest","publisher":{"@id":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion#creator"},"url":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion"},{"@id":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion#identity","@type":"NGO","address":{"@type":"PostalAddress","addressCountry":"US","addressLocality":"13th Floor","addressRegion":"New York","postalCode":"N.Y. 10013","streetAddress":"155 Avenue of the Americas"},"description":"ProPublica is an independent, non-profit newsroom that produces investigative journalism in the public interest.","email":"info@propublica.org","name":"ProPublica","sameAs":["https://twitter.com/propublica","https://www.facebookcorewwwi.onion/propublica/","https://en.wikipedia.org/wiki/ProPublica","https://www.youtube.com/user/propublica","https://github.com/propublica","https://www.linkedin.com/company/propublica/","https://www.instagram.com/propublica","https://www.pinterest.ie/propublica","https://vimeo.com/propublica"],"telephone":"1-212-514-5250","url":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion"},{"@id":"#creator","@type":"Organization"},{"@type":"BreadcrumbList","description":"Breadcrumbs list","itemListElement":[{"@type":"ListItem","item":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion","name":"Homepage","position":1}],"name":"Breadcrumbs"}]}</script>


SOUP:
<!DOCTYPE html>

<html class="no-js" lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- Critical JS -->
<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'enhanced')})(document.documentElement)</script>
<script>window.PP={};window.PP.utils={};window.PP.config={};PP.config.donateBanner={secondsUntilShown:0.25,daysForDonateBarDismissal:3};;(function(win,undefined){win.PP=win.PP||{};win.PP.utils=win.PP.utils||{};var PP=win.PP;// loadJS: load a JS file asynchronously. Included from https://github.com/filamentgroup/loadJS/
function loadJS(src,cb){"use strict";var ref=window.document.getElementsByTagName("script")[0];var script=window.document.createElement("script");script.src=src;script.async=true;ref.parentNode.insertBefore(script,ref);if(cb&&typeof cb==="function"){script.onload=cb}return script}PP.utils.loadJS=loadJS;// cookie function from https://github.com/filamentgroup/cookie/
function cookie(name,value,days){// if value is undefined, get the cookie value
if(value===undefined){var cookiestring="; "+window.document.cookie;var cookies=cookiestring.split("; "+name+"=");if(cookies.length===2){return cookies.pop().split(";").shift()}return null}else{var expires;var domain;// if value is a false boolean, we'll treat that as a delete
if(value===false){days=-1}if(days){var date=new Date;date.setTime(date.getTime()+days*24*60*60*1000);expires="; expires="+date.toGMTString()}else{expires=""}// check if we're in dev, if not, lets make it a domain-wide cookie
if(location.hostname=="localhost"||location.hostname=="127.0.0.1"){domain=""}else{domain="; domain=.propublica.org"}window.document.cookie=name+"="+value+expires+domain+"; path=/"}}// expose it
PP.utils.cookie=cookie})(this);;(function(win,undefined){// Enable JS strict mode
"use strict";// Environment variables
var PP=win.PP,utils=PP.utils,docClasses=[];// if ( "sessionStorage" in window ) {
//
//     // Font loading!
//     var fontsClass = "fonts-loaded";
//
//     if ( sessionStorage.getItem( "fontsLoaded" ) === "loaded" ) {
//         docClasses.push( fontsClass );
//     } else {
//         // Okay! Let’s watch for font events!
//         var tiemposText = new FontFaceObserver( "Tiempos Text", {
//             weight: 400
//         } );
//         var tiemposTextItalic = new FontFaceObserver( "Tiempos Text", {
//             style: "italic",
//             weight: 400
//         } );
//         var tiemposTextBold = new FontFaceObserver( "Tiempos Text", {
//             weight: 700
//         } );
//         var tiemposTextBoldItalic = new FontFaceObserver( "Tiempos Text", {
//             style: "italic",
//             weight: 700
//         } );
//         var graphik = new FontFaceObserver( "Graphik", {
//             weight: 400
//         } );
//         var graphikItalic = new FontFaceObserver( "Graphik", {
//             style: "italic",
//             weight: 400
//         } );
//         var graphikBold = new FontFaceObserver( "Graphik", {
//             weight: 700
//         } );
//         var graphikBoldItalic = new FontFaceObserver( "Graphik", {
//             style: "italic",
//             weight: 700
//         } );
//
//         // When the fonts above are loaded, we'll attach a .fonts-loaded class to the document. (Per https://www.filamentgroup.com/lab/font-events.html) We'll also append the class to the `html`.
//         win.Promise
//             .all( [
//                 tiemposText.load(),
//                 tiemposTextItalic.load(),
//                 tiemposTextBold.load(),
//                 tiemposTextBoldItalic.load(),
//                 graphik.load(),
//                 graphikItalic.load(),
//                 graphikBold.load(),
//                 graphikBoldItalic.load()
//             ] )
//             .then( function() {
//                 win.document.documentElement.className += " " + fontsClass;
//                 sessionStorage.setItem( "fontsLoaded" , "loaded" );
//             } );
//     }
// }
// Add scoping classes to HTML element
win.document.documentElement.className+=" "+docClasses.join(" ")})(this);
//# sourceMappingURL=initial.js.map
</script>
<!-- DFP JS -->
<script async="" src="https://htlbid.com/v3/propublica.org/htlbid.js"></script>
<script>window.htlbid = window.htlbid || {};
htlbid.cmd = htlbid.cmd || [];
htlbid.cmd.push(function() {
    htlbid.layout("universal"); // Leave as "universal" or add custom layout
        htlbid.setTargeting("is_testing", "no");
        htlbid.setTargeting("is_home", "no"); // Set to "yes" on the homepage
});
</script>
<!-- PRECONNECT -->
<link href="//assets.propub3r6espa33w.onion/static/prod/" rel="preconnect"/>
<link href="//assets.propub3r6espa33w.onion/static/prod/" rel="dns-prefetch"/>
<link href="https://www.facebook.com" rel="preconnect"/>
<link href="https://www.facebook.com" rel="dns-prefetch"/>
<link href="https://www.google.com" rel="preconnect"/>
<link href="https://www.google.com" rel="dns-prefetch"/>
<!-- STYLES -->
<link href="//assets.propub3r6espa33w.onion/static/prod/v4/css/main.1807583e.css" rel="stylesheet"/>
<!-- Chartbeat -->
<script>
        var _sf_async_config = window._sf_async_config = (window._sf_async_config || {});
        _sf_async_config.uid = 1577;
        _sf_async_config.domain = 'propublica.org';
        _sf_async_config.useCanonical = true;_sf_async_config.domain='propublica.org';
        _sf_async_config.useCanonicalDomain = true;
                        _sf_async_config.sections = 'The Insurrection';
                        _sf_async_config.authors = '';
    </script>
<!-- OTHER -->
<link href="//assets.propub3r6espa33w.onion/static/prod/v4/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="//assets.propub3r6espa33w.onion/static/prod/v4/images/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="//assets.propub3r6espa33w.onion/static/prod/v4/images/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="//assets.propub3r6espa33w.onion/static/prod/v4/images/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="//assets.propub3r6espa33w.onion/static/prod/v4/images/favicon-128x128.png" rel="icon" sizes="128x128" type="image/png"/>
<link href="//assets.propub3r6espa33w.onion/static/prod/v4/images/favicon-196x196.png" rel="icon" sizes="196x196" type="image/png"/>
<link href="//assets.propub3r6espa33w.onion/static/prod/v4/images/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<meta content="ProPublica" name="application-name"/>
<meta content="#FFFFFF" name="msapplication-TileColor"/>
<meta content="//assets.propub3r6espa33w.onion/static/prod/v4/images/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="//assets.propub3r6espa33w.onion/static/prod/v4/images/mstile-70x70.png" name="msapplication-square70x70logo"/>
<meta content="//assets.propub3r6espa33w.onion/static/prod/v4/images/mstile-150x150.png" name="msapplication-square150x150logo"/>
<meta content="//assets.propub3r6espa33w.onion/static/prod/v4/images/mstile-310x150.png" name="msapplication-wide310x150logo"/>
<meta content="//assets.propub3r6espa33w.onion/static/prod/v4/images/mstile-310x310.png" name="msapplication-square310x310logo"/>
<title>ProPublica — Investigative Journalism and News in the Public Interest</title>
<script>dataLayer = [];
</script><meta content="ProPublica is an independent, non-profit newsroom that produces investigative journalism in the public interest." name="description"/>
<meta content="no-referrer-when-downgrade" name="referrer"/>
<meta content="all" name="robots"/>
<meta content="13320939444" property="fb:profile_id"/>
<meta content="229862657130557" property="fb:app_id"/>
<meta content="en_US" property="og:locale"/>
<meta content="ProPublica" property="og:site_name"/>
<meta content="website" property="og:type"/>
<meta content="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/" property="og:url"/>
<meta content="Investigative Journalism and News in the Public Interest" property="og:title"/>
<meta content="ProPublica is an independent, non-profit newsroom that produces investigative journalism in the public interest." property="og:description"/>
<meta content="//assets.propub3r6espa33w.onion/2017-pp-open-graph-1200x630.jpg" property="og:image"/>
<meta content="https://vimeo.com/propublica" property="og:see_also"/>
<meta content="https://www.pinterest.ie/propublica" property="og:see_also"/>
<meta content="https://www.instagram.com/propublica" property="og:see_also"/>
<meta content="https://www.linkedin.com/company/propublica/" property="og:see_also"/>
<meta content="https://github.com/propublica" property="og:see_also"/>
<meta content="https://www.youtube.com/user/propublica" property="og:see_also"/>
<meta content="https://en.wikipedia.org/wiki/ProPublica" property="og:see_also"/>
<meta content="https://www.facebookcorewwwi.onion/propublica/" property="og:see_also"/>
<meta content="https://twitter.com/propublica" property="og:see_also"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@propublica" name="twitter:site"/>
<meta content="@propublica" name="twitter:creator"/>
<meta content="Investigative Journalism and News in the Public Interest" name="twitter:title"/>
<meta content="ProPublica is an independent, non-profit newsroom that produces investigative journalism in the public interest." name="twitter:description"/>
<meta content="//assets.propub3r6espa33w.onion/2017-pp-open-graph-1200x630.jpg" name="twitter:image"/>
<link href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion" rel="canonical"/>
<link href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion" rel="home"/>
<link href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/humans.txt" rel="author" type="text/plain"/></head>
<body class="home layout-grid" id="www-propublica-org">
<a class="a11y a11y-focusable skiplink" href="#main">Skip to main content</a>
<!-- SVG Sprite Sheet (sprites in alphabetical order) -->
<svg style="display: none;">
<!-- Close button light -->
<symbol id="icon-close-light" viewbox="0 0 24 24">
<title>Close</title>
<g fill="none" fill-rule="evenodd" stroke="none" stroke-linecap="square" stroke-width="1">
<g class="line" stroke="#FFFFFF" stroke-width="2" transform="translate(-1149.000000, -24.000000)">
<g id="Group-6" transform="translate(1150.000000, 25.000000)">
<path d="M0.393398282,21.6066017 L21.6066017,0.393398282" id="Line-lt"></path>
<path d="M0.393398282,21.6066017 L21.6066017,0.393398282" id="Line-lt-Copy" transform="translate(11.000000, 11.000000) scale(-1, 1) translate(-11.000000, -11.000000) "></path>
</g>
</g>
</g>
</symbol>
<!-- Close button dark -->
<symbol id="icon-close-dark" viewbox="0 0 24 24">
<title>Close</title>
<g fill="none" fill-rule="evenodd" stroke="none" stroke-linecap="square" stroke-width="1">
<g class="line" stroke="#304154" stroke-width="2" transform="translate(-1149.000000, -24.000000)">
<g id="Group-6-2" transform="translate(1150.000000, 25.000000)">
<path d="M0.393398282,21.6066017 L21.6066017,0.393398282" id="Line-dk"></path>
<path d="M0.393398282,21.6066017 L21.6066017,0.393398282" id="Line-dk-Copy" transform="translate(11.000000, 11.000000) scale(-1, 1) translate(-11.000000, -11.000000) "></path>
</g>
</g>
</g>
</symbol>
<!-- Comment -->
<symbol id="icon-comment" viewbox="0 0 24 24">
<title>Comment</title>
<path d="M23.05,6.71a8,8,0,0,1,0,7.58A9.86,9.86,0,0,1,20.5,17.4a12.6,12.6,0,0,1-3.83,2.07,14.1,14.1,0,0,1-4.67.77,15.91,15.91,0,0,1-2-.14Q6.83,23.25,3.5,23.25a.48.48,0,0,1-.48-.3.54.54,0,0,1,.12-.55l0,0a7.21,7.21,0,0,0,1.59-3,9,9,0,0,0,.19-.94A10.65,10.65,0,0,1,1.32,15,8.11,8.11,0,0,1,.95,6.71,9.87,9.87,0,0,1,3.5,3.6,12.6,12.6,0,0,1,7.33,1.52,14.1,14.1,0,0,1,12,.75a14.1,14.1,0,0,1,4.67.77A12.6,12.6,0,0,1,20.5,3.6,9.86,9.86,0,0,1,23.05,6.71Z"></path>
</symbol>
<!-- Creative Commons -->
<symbol id="icon-cc" viewbox="0 0 24 24">
<title>Creative Commons</title>
<path d="M12,0A11.56,11.56,0,0,0,3.56,3.49,12.07,12.07,0,0,0,0,12a11.78,11.78,0,0,0,3.52,8.45A11.9,11.9,0,0,0,12,24a12.21,12.21,0,0,0,8.59-3.58A11.55,11.55,0,0,0,24,12a11.76,11.76,0,0,0-3.46-8.48A11.87,11.87,0,0,0,12,0Zm0,2.17A9.77,9.77,0,0,1,21.83,12,9.37,9.37,0,0,1,19,18.87a10,10,0,0,1-7,2.93A9.7,9.7,0,0,1,5.1,18.9,9.76,9.76,0,0,1,2.17,12,9.91,9.91,0,0,1,5.1,5,9.45,9.45,0,0,1,12,2.17Z"></path><path d="M11.86,10A3.37,3.37,0,0,0,8.68,8.28,3.45,3.45,0,0,0,5.17,12a3.39,3.39,0,0,0,3.58,3.72,3.6,3.6,0,0,0,3.15-1.87l-1.49-.76a1.46,1.46,0,0,1-1.48,1c-1.1,0-1.61-.92-1.61-2.13s.43-2.13,1.61-2.13a1.52,1.52,0,0,1,1.33,1Z"></path><path d="M18.79,10a3.38,3.38,0,0,0-3.18-1.73A3.45,3.45,0,0,0,12.1,12a3.39,3.39,0,0,0,3.58,3.72,3.6,3.6,0,0,0,3.15-1.87l-1.49-.76a1.46,1.46,0,0,1-1.48,1c-1.1,0-1.61-.92-1.61-2.13s.43-2.13,1.61-2.13a1.52,1.52,0,0,1,1.33,1Z"></path>
</symbol>
<!-- Donate -->
<symbol id="icon-donate" viewbox="0 0 24 24">
<title>Donate</title>
<path d="M1,24H11V22H10a3,3,0,0,1-3-3H9a1,1,0,0,0,1,1h1V18H10a3,3,0,0,1,0-6h1V10H1Z"></path><path d="M9,15a1,1,0,0,0,1,1h1V14H10A1,1,0,0,0,9,15Z"></path><path d="M13,12h1a3,3,0,0,1,3,3H15a1,1,0,0,0-1-1H13v2h1a3,3,0,0,1,0,6H13v2H23V10H13Z"></path><path d="M15,19a1,1,0,0,0-1-1H13v2h1A1,1,0,0,0,15,19Z"></path><path d="M20.65,5A3.46,3.46,0,0,0,21,3.5,3.5,3.5,0,0,0,17.5,0,6.5,6.5,0,0,0,12,3.05,6.5,6.5,0,0,0,6.5,0,3.5,3.5,0,0,0,3,3.5,3.46,3.46,0,0,0,3.35,5H0V9H24V5ZM6.5,5a1.5,1.5,0,0,1,0-3,4.49,4.49,0,0,1,4.22,3Zm11,0H13.28A4.49,4.49,0,0,1,17.5,2a1.5,1.5,0,0,1,0,3Z"></path>
</symbol>
<!-- Email -->
<symbol id="icon-email" viewbox="0 0 24 24">
<title>Email</title>
<path d="M12,13.5,0,4.5A1.45,1.45,0,0,1,.44,3.44,1.44,1.44,0,0,1,1.5,3h21A1.5,1.5,0,0,1,24,4.5Zm0,2.7,12-9V19.5A1.5,1.5,0,0,1,22.5,21H1.5a1.44,1.44,0,0,1-1.06-.44A1.45,1.45,0,0,1,0,19.5V7.2Z"></path>
</symbol>
<!-- Email (add/subscribe) -->
<symbol id="icon-add-email" viewbox="0 0 24 24">
<title>Add Email</title>
<path d="M18.81,2a5.13,5.13,0,0,0-5.2,5.06A5.2,5.2,0,0,0,24,7.08,5.13,5.13,0,0,0,18.81,2Zm3.24,5.7h-2.6v2.53H18.16V7.72h-2.6V6.45h2.6V3.91h1.29V6.45h2.6Zm-2.6,5.65V20.8A1.2,1.2,0,0,1,18.24,22h-17A1.2,1.2,0,0,1,0,20.8V11l9.73,7.14,7-5.12a6.89,6.89,0,0,0,2.1.34C19,13.4,19.24,13.39,19.45,13.37ZM9.73,16,0,8.9A1.2,1.2,0,0,1,1.22,7.72H12.35A6.28,6.28,0,0,0,15,12.18Z"></path>
</symbol>
<!-- Facebook -->
<symbol id="icon-facebook" viewbox="0 0 24 24">
<title>Facebook</title>
<path d="M24,1.5v21A1.52,1.52,0,0,1,22.5,24h-6V14.67h3l.5-3.46H16.5V8.5a1.6,1.6,0,0,1,.38-1.15A1.48,1.48,0,0,1,18,6.95h2.25V3.83a15.64,15.64,0,0,0-2.46-.16A5.11,5.11,0,0,0,14.1,4.89a4.41,4.41,0,0,0-1.27,3.33v3h-3v3.46h3V24H1.5a1.44,1.44,0,0,1-1.05-.45A1.44,1.44,0,0,1,0,22.5V1.5A1.44,1.44,0,0,1,.45.45,1.44,1.44,0,0,1,1.5,0h21a1.44,1.44,0,0,1,1.05.45A1.44,1.44,0,0,1,24,1.5Z"></path>
</symbol>
<!-- Instagram -->
<symbol id="icon-instagram" viewbox="0 0 24 24">
<title>Instagram</title>
<path d="M12,2.32c3.15,0,3.53,0,4.77.07a6.51,6.51,0,0,1,2.19.4,3.58,3.58,0,0,1,1.36.89A3.58,3.58,0,0,1,21.21,5a6.51,6.51,0,0,1,.4,2.19c.06,1.24.07,1.62.07,4.77s0,3.53-.07,4.77a6.51,6.51,0,0,1-.4,2.19A4,4,0,0,1,19,21.21a6.51,6.51,0,0,1-2.19.4c-1.24.06-1.62.07-4.77.07s-3.53,0-4.77-.07A6.51,6.51,0,0,1,5,21.21a3.58,3.58,0,0,1-1.36-.89A3.58,3.58,0,0,1,2.79,19a6.51,6.51,0,0,1-.4-2.19c-.06-1.24-.07-1.62-.07-4.77s0-3.53.07-4.77A6.51,6.51,0,0,1,2.79,5a3.58,3.58,0,0,1,.89-1.36A3.58,3.58,0,0,1,5,2.79a6.51,6.51,0,0,1,2.19-.4c1.24-.06,1.62-.07,4.77-.07M12,.19C8.79.19,8.39.2,7.13.26A8.84,8.84,0,0,0,4.26.81,6,6,0,0,0,.81,4.26,8.84,8.84,0,0,0,.26,7.13C.2,8.39.19,8.79.19,12s0,3.61.07,4.87a8.84,8.84,0,0,0,.55,2.87,6,6,0,0,0,3.45,3.45,8.84,8.84,0,0,0,2.87.55c1.26.06,1.66.07,4.87.07s3.61,0,4.87-.07a8.84,8.84,0,0,0,2.87-.55,6,6,0,0,0,3.45-3.45,8.84,8.84,0,0,0,.55-2.87c.06-1.26.07-1.66.07-4.87s0-3.61-.07-4.87a8.84,8.84,0,0,0-.55-2.87A6,6,0,0,0,19.74.81,8.84,8.84,0,0,0,16.87.26C15.61.2,15.21.19,12,.19Z"></path><path d="M12,5.94A6.06,6.06,0,1,0,18.06,12,6.06,6.06,0,0,0,12,5.94Zm0,10A3.94,3.94,0,1,1,15.94,12,3.95,3.95,0,0,1,12,15.94Z"></path><circle cx="18.3" cy="5.7" r="1.42"></circle>
</symbol>
<!-- Messenger -->
<symbol id="icon-messenger" viewbox="0 0 22 22">
<title>Facebook Messenger</title>
<path d="M11,0 C4.92620865,0 0,4.55793451 0,10.1826196 C0,13.3828715 1.59262087,16.2367758 4.08651399,18.1042821 L4.08651399,22 L7.83994911,19.938539 C8.84198473,20.2156171 9.9,20.3652393 11,20.3652393 C17.0737913,20.3652393 22,15.8073048 22,10.1826196 C22,4.55793451 17.0737913,0 11,0 Z M12.1587786,13.6544081 L9.31221374,10.7284635 L3.8346056,13.7292191 L9.84122137,7.42015113 L12.6849873,10.3460957 L18.1625954,7.34534005 L12.1587786,13.6544081 Z" fill="#FFFFFF" fill-rule="nonzero" id="Facebook-Messenger"></path>
</symbol>
<!-- Mobile -->
<symbol id="icon-mobile" viewbox="0 0 24 24">
<title>Mobile</title>
<path d="M18,0a1.45,1.45,0,0,1,1.06.44A1.45,1.45,0,0,1,19.5,1.5v21A1.5,1.5,0,0,1,18,24H6a1.5,1.5,0,0,1-1.5-1.5V1.5A1.45,1.45,0,0,1,4.94.44,1.44,1.44,0,0,1,6,0ZM17,18V4H7V18Zm-6.06,4.06A1.5,1.5,0,1,0,10.5,21,1.45,1.45,0,0,0,10.94,22.06Z"></path>
</symbol>
<!-- Nav menu button (hamburger) -->
<symbol id="icon-nav-menu" viewbox="0 0 24 24">
<title>Nav Menu</title>
<path d="M24,5.5a.5.5,0,0,1-.5.5H.5A.5.5,0,0,1,0,5.5v-4A.5.5,0,0,1,.5,1h23a.5.5,0,0,1,.5.5Zm0,9a.5.5,0,0,1-.5.5H.5a.5.5,0,0,1-.5-.5v-4A.5.5,0,0,1,.5,10h23a.5.5,0,0,1,.5.5Zm0,9a.5.5,0,0,1-.5.5H.5a.5.5,0,0,1-.5-.5v-4A.5.5,0,0,1,.5,19h23a.5.5,0,0,1,.5.5Z"></path>
</symbol>
<!-- Podcast -->
<symbol id="icon-podcast" viewbox="0 0 24 24">
<title>Podcast</title>
<path d="M10,22a.48.48,0,0,1-.35-.14L4.29,16.5H.51a.49.49,0,0,1-.36-.15A.49.49,0,0,1,0,16V8a.49.49,0,0,1,.15-.36A.49.49,0,0,1,.51,7.5H4.29L9.65,2.14a.5.5,0,0,1,.86.35v19a.45.45,0,0,1-.3.46A.63.63,0,0,1,10,22Zm5.2-2a.45.45,0,0,1-.35.15A.49.49,0,0,1,14.5,20l-1.42-1.42a.51.51,0,0,1-.14-.36.47.47,0,0,1,.14-.35,8.09,8.09,0,0,0,2.14-3.71,8.18,8.18,0,0,0,0-4.25,8.09,8.09,0,0,0-2.14-3.71.47.47,0,0,1-.14-.35.51.51,0,0,1,.14-.36L14.5,4a.49.49,0,0,1,.36-.15.45.45,0,0,1,.35.15,11,11,0,0,1,2.46,3.72,11.06,11.06,0,0,1,0,8.47A11,11,0,0,1,15.21,20Zm3.19,3.89L17,22.43a.51.51,0,0,1-.14-.36.47.47,0,0,1,.14-.35,13.78,13.78,0,0,0,2.58-3.56,13,13,0,0,0,1.29-4.07,15.09,15.09,0,0,0,0-4.2,13,13,0,0,0-1.29-4.07A13.78,13.78,0,0,0,17,2.27a.47.47,0,0,1-.14-.35A.51.51,0,0,1,17,1.57L18.39.15A.51.51,0,0,1,18.75,0a.47.47,0,0,1,.35.14,17,17,0,0,1,2.72,3.57,16.43,16.43,0,0,1,1.65,4,17.24,17.24,0,0,1,0,8.51,16.43,16.43,0,0,1-1.65,4,17,17,0,0,1-2.72,3.57.47.47,0,0,1-.35.14A.51.51,0,0,1,18.39,23.85Z"></path>
</symbol>
<!-- Print -->
<symbol id="icon-print" viewbox="0 0 24 24">
<title>Print</title>
<path d="M22.5,0a1.45,1.45,0,0,1,1.06.44A1.45,1.45,0,0,1,24,1.5v12A1.5,1.5,0,0,1,22.5,15H20V10H4v5H1.5a1.45,1.45,0,0,1-1.06-.44A1.45,1.45,0,0,1,0,13.5V1.5A1.45,1.45,0,0,1,.44.44,1.45,1.45,0,0,1,1.5,0ZM6,12V23.5a.5.5,0,0,0,.5.5h11a.5.5,0,0,0,.5-.5V12ZM18.44,5.56A1.5,1.5,0,0,0,21,4.5a1.5,1.5,0,0,0-3,0A1.45,1.45,0,0,0,18.44,5.56Z"></path>
</symbol>
<!-- ProPublica Logo -->
<symbol id="propublica-logo" viewbox="0 0 574.24 75">
<title>ProPublica</title>
<path class="cls-1" d="M66.4,13.48,66,14.91l6.23,11.45,1.15.47L75,26.29V75H50.95A41.2,41.2,0,0,0,62.13,46.88a40.61,40.61,0,0,0-3.39-16.26,45.17,45.17,0,0,1,7.79-4.95l1.69-.2.14-.41L65,18.83H64.5l-.88,1.29c-2.91,1.9-4.67,2.3-8.54,3.79A41.35,41.35,0,0,0,20.46,5.42,39.87,39.87,0,0,0,0,10.91V0H75V8.81ZM57.32,47A37.54,37.54,0,0,1,44.65,75H0V16A37,37,0,0,1,57.32,47ZM43.7,37.06c0-8.13-6-12-17.75-12H7.18v3.66l4.2.95V62.2l-4.2.95v3.73H27.85V63.14l-7.18-.95V50.14h5.15C37.26,50.14,43.7,45.53,43.7,37.06Zm-9.76.27c0,5.15-2.78,8.33-7.25,8.33h-6V29.47H27C31.37,29.47,33.94,32.32,33.94,37.33Z"></path><path class="cls-1" d="M116.53,60.07l9.15,1.08v4.2H99.39v-4.2l5.28-1.36v-44l-5.28-1.36V10.41H123c15.31,0,22.9,5.28,22.9,16.13,0,11.18-8.33,17.41-23.1,17.41h-6.3Zm0-21.14h6.84c6.44,0,10.5-4.61,10.5-12.06,0-7.18-3.73-11.45-10.09-11.45h-7.25Z"></path><path class="cls-1" d="M200.74,61.15v4.2H185.9l-16-20.6h-2.51v15l5.28,1.36v4.2H150.27v-4.2l5.28-1.36V23.35l-5-1.36V17.93H175.2c10.5,0,17.55,5.08,17.55,12.67,0,8.4-5.76,12.2-11,13.55l13.48,15.79ZM167.41,39.74h4.47c5.76,0,8.81-3.18,8.81-8.88,0-5.35-2.85-7.93-8.81-7.93h-4.47Z"></path><path class="cls-1" d="M199.59,41.64c0-14.77,9.82-24.73,25.68-24.73s25.68,10,25.68,24.73-9.82,24.73-25.68,24.73S199.59,56.48,199.59,41.64Zm38.89,0c0-9.69-3.25-19.17-13.21-19.17S212.05,32,212.05,41.64s3.25,19.17,13.21,19.17S238.48,51.33,238.48,41.64Z"></path><path class="cls-1" d="M270.79,60.07l9.15,1.08v4.2H253.31v-4.2l5.62-1.36v-44l-5.62-1.36V10.41h24c15.31,0,22.9,5.28,22.9,16.13,0,11.18-8.33,17.41-23.1,17.41h-6.3Zm0-21.14h6.84c6.44,0,10.5-4.61,10.5-12.06,0-7.18-3.73-11.45-10.1-11.45h-7.25Z"></path><path class="cls-1" d="M354.26,22l-4.95,1.36V47c0,12.87-7.79,19.38-20.05,19.38-13.62,0-20.73-7.86-20.73-20v-23L303.58,22V17.93h22V22l-5.08,1.36V47.74c0,7.25,4.34,12.06,11.45,12.06,6.84,0,11.25-4.47,11.25-11.31V23.35L338.14,22V17.93h16.12Z"></path><path class="cls-1" d="M403.85,52.21c0,8.88-7.11,13.14-22,13.14H357.58v-4.2l5.56-1.36V23.35L358.26,22V17.93h23.92c12.6,0,19.51,3.79,19.51,11.72,0,6.17-4.06,9.08-10.91,9.89v.27C399.31,40.76,403.85,45.1,403.85,52.21ZM375,38.32h5.76c6.1,0,9-2.78,9-7.79,0-5.22-3.12-7.59-9-7.59H375Zm16.8,13.41c0-5.62-3.79-8.67-11.25-8.67H375V60.34h6.64C388.07,60.34,391.79,57.29,391.79,51.74Z"></path><path class="cls-1" d="M451.07,48.62l-4.4,16.73H408v-4.2l5.28-1.36V23.35L407.65,22V17.93h22.76V22l-5.28,1.36v37l14.16-.81L447,47.26Z"></path><path class="cls-1" d="M475.4,61.15v4.2H453v-4.2l5.28-1.36V23.35L453,22V17.93H475.4V22l-5.28,1.36V59.8Z"></path><path class="cls-1" d="M478,41.78c0-15.85,12.13-24.86,27-24.86a35.6,35.6,0,0,1,18.29,5.15l-2,13.21h-4.2l-1.69-9.08a13.23,13.23,0,0,0-10.09-4.13c-9.15,0-14.7,6.57-14.7,18.22,0,13.21,6.84,19.31,15.24,19.31,6.17,0,10.23-3.39,13.41-9.55l4.27,2c-4.27,10-11.65,14.36-21.07,14.36C488.4,66.37,478,56.48,478,41.78Z"></path><path class="cls-1" d="M574.24,61.15v4.2H551.48v-4.2L557,60l-2.85-8.13h-16.6l-3.12,8.2,5.42,1.15-.07,4.2-16.53-.07v-4.2l4.27-1.08,14.9-37.33-4-1.29V17.93h13.69L569.3,60Zm-27.57-31-.47-1.56-.47,1.56-6.23,16.73h12.94Z"></path>
</symbol>
<!-- RSS -->
<symbol id="icon-rss" viewbox="0 0 24 24">
<title>RSS</title>
<path d="M24,23.5a.5.5,0,0,1-.5.5H20a.5.5,0,0,1-.5-.5,18.88,18.88,0,0,0-.68-5.05,18.84,18.84,0,0,0-1.9-4.55,19.06,19.06,0,0,0-3-3.84,19.06,19.06,0,0,0-3.84-3,18.84,18.84,0,0,0-4.55-1.9A18.88,18.88,0,0,0,.5,4.5a.49.49,0,0,1-.36-.15A.49.49,0,0,1,0,4V.5A.49.49,0,0,1,.15.15.49.49,0,0,1,.5,0,23.23,23.23,0,0,1,6.74.84a23.3,23.3,0,0,1,5.62,2.36,23.69,23.69,0,0,1,4.75,3.69,23.69,23.69,0,0,1,3.69,4.75,23.3,23.3,0,0,1,2.36,5.62A23.23,23.23,0,0,1,24,23.5Zm-7.5,0a.5.5,0,0,1-.5.5H12.5a.5.5,0,0,1-.5-.5A11.27,11.27,0,0,0,11.09,19a11.52,11.52,0,0,0-2.45-3.67A11.52,11.52,0,0,0,5,12.91,11.27,11.27,0,0,0,.5,12a.49.49,0,0,1-.36-.15A.49.49,0,0,1,0,11.5V8a.49.49,0,0,1,.15-.36A.49.49,0,0,1,.5,7.5a15.83,15.83,0,0,1,4.24.57A15.94,15.94,0,0,1,8.58,9.68a16,16,0,0,1,3.23,2.51,16,16,0,0,1,2.51,3.23,15.94,15.94,0,0,1,1.61,3.83A15.83,15.83,0,0,1,16.5,23.5ZM6.41,17.59a3.79,3.79,0,0,1,0,5.32A3.62,3.62,0,0,1,3.75,24a3.62,3.62,0,0,1-2.66-1.09,3.79,3.79,0,0,1,0-5.32A3.62,3.62,0,0,1,3.75,16.5,3.62,3.62,0,0,1,6.41,17.59Z"></path>
</symbol>
<!-- Search -->
<symbol id="icon-search" viewbox="0 0 24 24">
<title>Search</title>
<path d="M10.48,1.21a9.06,9.06,0,0,1,9,0,9,9,0,0,1,3.28,3.28A8.8,8.8,0,0,1,24,9a8.8,8.8,0,0,1-1.21,4.52,9,9,0,0,1-3.28,3.28A8.8,8.8,0,0,1,15,18a8.7,8.7,0,0,1-4.8-1.39L3.42,23.41A2,2,0,0,1,2,24a1.93,1.93,0,0,1-1.42-.59,2,2,0,0,1,0-2.84L7.39,13.8A8.7,8.7,0,0,1,6,9,8.8,8.8,0,0,1,7.21,4.48,9,9,0,0,1,10.48,1.21Zm2.19,13.32a5.94,5.94,0,0,0,4.65,0,5.94,5.94,0,0,0,3.2-3.2,5.94,5.94,0,0,0,0-4.65,5.94,5.94,0,0,0-3.2-3.2,5.94,5.94,0,0,0-4.65,0,5.94,5.94,0,0,0-3.2,3.2,5.94,5.94,0,0,0,0,4.65,5.94,5.94,0,0,0,3.2,3.2Z"></path>
</symbol>
<!-- Secure -->
<symbol id="icon-secure" viewbox="0 0 24 24">
<title>Secure</title>
<path d="M19.5,10.5A1.5,1.5,0,0,1,21,12V22.5A1.5,1.5,0,0,1,19.5,24H4.5A1.5,1.5,0,0,1,3,22.5V12a1.5,1.5,0,0,1,1.5-1.5H6V6a5.84,5.84,0,0,1,.47-2.33A5.94,5.94,0,0,1,9.67.47a5.94,5.94,0,0,1,4.65,0,5.94,5.94,0,0,1,3.2,3.2A5.84,5.84,0,0,1,18,6v4.5ZM9,6v4.5h6V6a2.89,2.89,0,0,0-.88-2.12,3,3,0,0,0-4.24,0A2.89,2.89,0,0,0,9,6Z"></path>
</symbol>
<!-- Twitter -->
<symbol id="icon-twitter" viewbox="0 0 24 24">
<title>Twitter</title>
<path d="M24,4.55A9.88,9.88,0,0,1,21.55,7.1v.64a13.51,13.51,0,0,1-.42,3.3,15.87,15.87,0,0,1-1.2,3.22,13.16,13.16,0,0,1-2,2.92,14.5,14.5,0,0,1-2.73,2.37,12.7,12.7,0,0,1-3.49,1.6,14.57,14.57,0,0,1-4.17.59,13.69,13.69,0,0,1-7.56-2,7.81,7.81,0,0,0,1.17.08,8.52,8.52,0,0,0,3.21-.63,11.36,11.36,0,0,0,2.89-1.7,4.66,4.66,0,0,1-2.85-1A4.79,4.79,0,0,1,2.7,14.07,4.78,4.78,0,0,0,4.92,14,4.8,4.8,0,0,1,2.1,12.3,4.76,4.76,0,0,1,1,9.16V9.11a4.91,4.91,0,0,0,2.23.61A4.94,4.94,0,0,1,1.59,8,4.75,4.75,0,0,1,1,5.64a4.82,4.82,0,0,1,.67-2.39A13.69,13.69,0,0,0,6.14,6.81,14.18,14.18,0,0,0,11.8,8.3a5.16,5.16,0,0,1-.11-1.14,4.73,4.73,0,0,1,1.44-3.47,4.73,4.73,0,0,1,3.47-1.44A4.91,4.91,0,0,1,20.23,3.8a9.79,9.79,0,0,0,3.11-1.2,4.72,4.72,0,0,1-2.17,2.73A9.36,9.36,0,0,0,24,4.55Z"></path>
</symbol>
<!-- WhatsApp -->
<svg id="icon-whatsapp" viewbox="0 0 25 24">
<title>WhatsApp</title>
<path d="M20.7589168,3.489665 C18.4736378,1.24305061 15.4401331,0.005702067 12.2036214,0 C5.53339105,0 0.110203805,5.33713471 0.104403605,11.8888097 C0.104403605,13.9871703 0.66122283,16.0285103 1.71685928,17.8360656 L0,24 L6.41502149,22.3464006 C8.18408258,23.2929437 10.1735513,23.7947256 12.1978212,23.7947256 L12.2036214,23.7947256 C12.2036214,23.7947256 12.2036214,23.7947256 12.2036214,23.7947256 C18.8680515,23.7947256 24.2970389,18.4575909 24.3028391,11.9002138 C24.3028391,8.72416251 23.0441957,5.7362794 20.7589168,3.489665 Z M12.2036214,21.787598 C10.3939589,21.787598 8.6248978,21.3086244 7.08204453,20.4076978 L6.71663191,20.1910192 L2.91170053,21.1717748 L3.92673558,17.5224519 L3.68892737,17.1461155 C2.67969252,15.572345 2.1518743,13.7533856 2.1518743,11.8888097 C2.1518743,6.43763364 6.66443011,2.00712758 12.2094216,2.00712758 C14.8949143,2.00712758 17.4180014,3.03920171 19.3204671,4.90377762 C21.2171326,6.7740556 22.2611686,9.25445474 22.2611686,11.8945118 C22.2553684,17.3513899 17.7428126,21.787598 12.2036214,21.787598 Z M17.7196118,14.380613 C17.4180014,14.2323592 15.9331501,13.5138988 15.6547405,13.4112616 C15.3763309,13.3143264 15.1733239,13.2630078 14.9761171,13.5595153 C14.7731101,13.8560228 14.1930901,14.5288667 14.019084,14.722737 C13.845078,14.9223093 13.6652718,14.9451176 13.3636614,14.7968639 C13.062051,14.6486101 12.0876174,14.3349964 10.9333775,13.3200285 C10.0343465,12.5331433 9.43112563,11.5580898 9.25131943,11.2615823 C9.07731342,10.9650748 9.23391882,10.805417 9.38472403,10.6571632 C9.51812864,10.5260157 9.68633445,10.3093371 9.83713965,10.1382751 C9.98794486,9.96721311 10.0401467,9.84176764 10.1387501,9.6421953 C10.2373535,9.44262295 10.1909519,9.27156094 10.1155493,9.1233072 C10.0401467,8.97505346 9.43692583,7.50962224 9.18171702,6.91660727 C8.93810861,6.33499644 8.6887,6.41482537 8.50309359,6.40912331 C8.32908758,6.39771917 8.12608057,6.39771917 7.92307356,6.39771917 C7.72006655,6.39771917 7.39525534,6.47184604 7.11684573,6.76835353 C6.83843611,7.06486101 6.06120928,7.78332145 6.06120928,9.24875267 C6.06120928,10.7141839 7.14584673,12.1225944 7.29665194,12.3221668 C7.44745714,12.5217391 9.42532543,15.5210264 12.4588302,16.8096935 C13.178055,17.1176051 13.7406744,17.3000713 14.1814897,17.4369209 C14.9065147,17.6650036 15.5619373,17.6307912 16.0839553,17.5566643 C16.6639754,17.4711333 17.870417,16.8382038 18.1256258,16.1425517 C18.3750344,15.4468995 18.3750344,14.8538845 18.2996318,14.7284391 C18.2242292,14.6029936 18.0212222,14.5288667 17.7196118,14.380613 Z"></path>
</svg>
<!-- YouTube -->
<symbol id="icon-youtube" viewbox="0 0 24 24">
<title>YouTube</title>
<path d="M24,12c0,8.5,0,8.5-12,8.5S0,20.5,0,12,0,3.5,12,3.5,24,3.5,24,12Zm-7.5,0L9,7.5v9Z"></path>
</symbol>
</svg>
<div class="masthead-wrapper">
<div class="masthead-inner content">
<header class="masthead" role="banner">
<nav aria-label="Site Navigation" class="org-nav" role="navigation">
<ul>
<li class="org-nav-item main"><a href="/">ProPublica</a></li>
<li class="org-nav-item local-initiatives"><a href="/local-initiatives/">Local Initiatives</a></li>
<li class="org-nav-item data-store"><a href="/datastore/">Data Store</a></li>
</ul>
</nav>
<!-- end .org-nav -->
<nav aria-label="Action navigation" class="action-nav">
<ul>
<li class="action-nav-item donate">
<a href="/donate">Donate</a>
</li>
<li class="action-nav-item social twitter">
<a href="https://twitter.com/propublica">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-twitter"></use>
</svg>
<span class="cta">Follow us on Twitter</span>
</a>
</li>
<li class="action-nav-item social facebook">
<a href="https://www.facebookcorewwwi.onion/propublica">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-facebook"></use>
</svg>
<span class="cta">Like us on Facebook</span>
</a>
</li>
<li class="action-nav-item tools search">
<a data-collapsible-target="#modal-search" href="/search/">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-search"></use>
</svg>
<span class="cta">Search</span>
</a>
</li>
<li class="action-nav-item tools newsletters">
<a href="/newsletters/">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-add-email"></use>
</svg>
<span class="cta">Newsletters</span>
</a>
</li>
</ul>
</nav>
<!-- end .action-nav -->
<div class="masthead-inner-wrapper">
<h1>
<a class="logo" data-title="Investigative Journalism and News in the Public Interest" href="/">
<svg data-name="propublica-wordmark" id="propublica-wordmark" viewbox="0 0 574.24 75" xmlns="http://www.w3.org/2000/svg"><title>ProPublica logo</title><path class="cls-1" d="M66.4,13.48,66,14.91l6.23,11.45,1.15.47L75,26.29V75H50.95A41.2,41.2,0,0,0,62.13,46.88a40.61,40.61,0,0,0-3.39-16.26,45.17,45.17,0,0,1,7.79-4.95l1.69-.2.14-.41L65,18.83H64.5l-.88,1.29c-2.91,1.9-4.67,2.3-8.54,3.79A41.35,41.35,0,0,0,20.46,5.42,39.87,39.87,0,0,0,0,10.91V0H75V8.81ZM57.32,47A37.54,37.54,0,0,1,44.65,75H0V16A37,37,0,0,1,57.32,47ZM43.7,37.06c0-8.13-6-12-17.75-12H7.18v3.66l4.2.95V62.2l-4.2.95v3.73H27.85V63.14l-7.18-.95V50.14h5.15C37.26,50.14,43.7,45.53,43.7,37.06Zm-9.76.27c0,5.15-2.78,8.33-7.25,8.33h-6V29.47H27C31.37,29.47,33.94,32.32,33.94,37.33Z"></path><path class="cls-1" d="M116.53,60.07l9.15,1.08v4.2H99.39v-4.2l5.28-1.36v-44l-5.28-1.36V10.41H123c15.31,0,22.9,5.28,22.9,16.13,0,11.18-8.33,17.41-23.1,17.41h-6.3Zm0-21.14h6.84c6.44,0,10.5-4.61,10.5-12.06,0-7.18-3.73-11.45-10.09-11.45h-7.25Z"></path><path class="cls-1" d="M200.74,61.15v4.2H185.9l-16-20.6h-2.51v15l5.28,1.36v4.2H150.27v-4.2l5.28-1.36V23.35l-5-1.36V17.93H175.2c10.5,0,17.55,5.08,17.55,12.67,0,8.4-5.76,12.2-11,13.55l13.48,15.79ZM167.41,39.74h4.47c5.76,0,8.81-3.18,8.81-8.88,0-5.35-2.85-7.93-8.81-7.93h-4.47Z"></path><path class="cls-1" d="M199.59,41.64c0-14.77,9.82-24.73,25.68-24.73s25.68,10,25.68,24.73-9.82,24.73-25.68,24.73S199.59,56.48,199.59,41.64Zm38.89,0c0-9.69-3.25-19.17-13.21-19.17S212.05,32,212.05,41.64s3.25,19.17,13.21,19.17S238.48,51.33,238.48,41.64Z"></path><path class="cls-1" d="M270.79,60.07l9.15,1.08v4.2H253.31v-4.2l5.62-1.36v-44l-5.62-1.36V10.41h24c15.31,0,22.9,5.28,22.9,16.13,0,11.18-8.33,17.41-23.1,17.41h-6.3Zm0-21.14h6.84c6.44,0,10.5-4.61,10.5-12.06,0-7.18-3.73-11.45-10.1-11.45h-7.25Z"></path><path class="cls-1" d="M354.26,22l-4.95,1.36V47c0,12.87-7.79,19.38-20.05,19.38-13.62,0-20.73-7.86-20.73-20v-23L303.58,22V17.93h22V22l-5.08,1.36V47.74c0,7.25,4.34,12.06,11.45,12.06,6.84,0,11.25-4.47,11.25-11.31V23.35L338.14,22V17.93h16.12Z"></path><path class="cls-1" d="M403.85,52.21c0,8.88-7.11,13.14-22,13.14H357.58v-4.2l5.56-1.36V23.35L358.26,22V17.93h23.92c12.6,0,19.51,3.79,19.51,11.72,0,6.17-4.06,9.08-10.91,9.89v.27C399.31,40.76,403.85,45.1,403.85,52.21ZM375,38.32h5.76c6.1,0,9-2.78,9-7.79,0-5.22-3.12-7.59-9-7.59H375Zm16.8,13.41c0-5.62-3.79-8.67-11.25-8.67H375V60.34h6.64C388.07,60.34,391.79,57.29,391.79,51.74Z"></path><path class="cls-1" d="M451.07,48.62l-4.4,16.73H408v-4.2l5.28-1.36V23.35L407.65,22V17.93h22.76V22l-5.28,1.36v37l14.16-.81L447,47.26Z"></path><path class="cls-1" d="M475.4,61.15v4.2H453v-4.2l5.28-1.36V23.35L453,22V17.93H475.4V22l-5.28,1.36V59.8Z"></path><path class="cls-1" d="M478,41.78c0-15.85,12.13-24.86,27-24.86a35.6,35.6,0,0,1,18.29,5.15l-2,13.21h-4.2l-1.69-9.08a13.23,13.23,0,0,0-10.09-4.13c-9.15,0-14.7,6.57-14.7,18.22,0,13.21,6.84,19.31,15.24,19.31,6.17,0,10.23-3.39,13.41-9.55l4.27,2c-4.27,10-11.65,14.36-21.07,14.36C488.4,66.37,478,56.48,478,41.78Z"></path><path class="cls-1" d="M574.24,61.15v4.2H551.48v-4.2L557,60l-2.85-8.13h-16.6l-3.12,8.2,5.42,1.15-.07,4.2-16.53-.07v-4.2l4.27-1.08,14.9-37.33-4-1.29V17.93h13.69L569.3,60Zm-27.57-31-.47-1.56-.47,1.56-6.23,16.73h12.94Z"></path></svg>
</a>
</h1>
<!-- end .logo -->
<nav aria-label="Primary navigation" class="primary-nav">
<ul>
<li class="primary-nav-item news-apps squelch"><a href="/newsapps/">Graphics &amp; Data</a></li>
<li class="primary-nav-item newsletters"><a href="/newsletters/">Newsletters</a></li>
<li class="primary-nav-item about"><a href="/about/">About</a></li>
</ul>
</nav>
<!-- end .primary-nav -->
<div class="email-nav">
<form action="https://signup.propublica.org/newsletter/turing" class="ajax-form" id="newsletter-header-form" method="post">
<div class="input-wrapper">
<label class="a11y" for="pp-email-signup--lead">Email Address</label>
<input class="text subscribe-email-text" id="pp-email-signup--lead" name="email" onfocus="recaptchaInit()" placeholder="Get the Big Story" type="email"/>
<input class="btn subscribe-email-btn g-recaptcha" id="newsletter-signup-header" type="submit" value="Join"/>
<input name="target" type="hidden" value="l/125411/2018-11-01/5vd2wz"/>
<input name="stlist" type="hidden" value="The Big Story"/>
<input name="source" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/"/>
<input name="placement" type="hidden" value="main-nav"/>
<input name="region" type="hidden" value="national"/>
<input name="success_location" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/forms/thankyou"/>
<input name="origin_location" type="hidden" value="https://v3-www.propublica.org/"/>
<input name="error_location" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/forms/error"/>
<svg class="icon">
<use xlink:href="#icon-add-email"></use>
</svg>
</div>
</form>
</div>
<!-- end .email-nav -->
</div>
<!-- end .masthead-inner-wrapper -->
<nav aria-label="Topuc navigation" class="secondary-nav">
<ul class="topics-nav">
<li class="topics-nav-item">
<a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/topics/racial-justice">
<svg class="topic-icon" viewbox="0 0 15 15"><path d="M15,12.23v0L14,4.55l.24.06.11,0a.38.38,0,0,0,.36-.27.38.38,0,0,0-.25-.47c-.19-.06-.39-.1-.59-.15a.38.38,0,0,0-.29-.26.37.37,0,0,0-.28.11c-.6-.15-1.21-.28-1.82-.39V1.29A1.29,1.29,0,0,0,10.17,0,1.32,1.32,0,0,0,9.11.57a1.37,1.37,0,0,0-2,.26A1.3,1.3,0,0,0,6.23.51a1.32,1.32,0,0,0-1.12.66A1.13,1.13,0,0,0,4.46,1,1.19,1.19,0,0,0,3.27,2.14V3.25c-.53.1-1,.21-1.57.34a.37.37,0,0,0-.28-.11.38.38,0,0,0-.29.26c-.19,0-.39.09-.58.15a.38.38,0,0,0-.25.47.38.38,0,0,0,.36.27l.11,0L1,4.55,0,12.21v0A2.67,2.67,0,0,0,2.6,15a2.67,2.67,0,0,0,2.57-2.73h0a.19.19,0,0,0,0-.07.13.13,0,0,0,0-.06h0L2,4.27c.41-.1.82-.18,1.24-.25v.37a4,4,0,0,0-.12,1A3.93,3.93,0,0,0,5.21,8.81v1.77a.38.38,0,1,0,.75,0v-2a.39.39,0,0,0-.21-.34A3.19,3.19,0,0,1,3.9,5.39a1.18,1.18,0,0,0,.56.16,1.2,1.2,0,0,0,.8-.32,1.35,1.35,0,0,0,1,.43,1.29,1.29,0,0,0,1-.44,1.29,1.29,0,0,0,.6.32,3.58,3.58,0,0,0-.89.85.37.37,0,0,0,.3.59.39.39,0,0,0,.31-.15A2.81,2.81,0,0,1,9.76,5.61a.38.38,0,0,0,0-.75H8.14a.59.59,0,0,1-.58-.59v-.5h2.82a.73.73,0,0,1,.75.71v.89A3.19,3.19,0,0,1,9.28,8.24a.39.39,0,0,0-.21.34v2a.38.38,0,1,0,.75,0V8.81a3.93,3.93,0,0,0,2.06-3.44V4.48A1.33,1.33,0,0,0,11.8,4c.4.07.79.15,1.18.24L9.87,12.11h0a.13.13,0,0,0,0,.06.25.25,0,0,0,0,.07h0A2.67,2.67,0,0,0,12.41,15,2.67,2.67,0,0,0,15,12.25ZM1.65,5.35l2.59,6.53H.83Zm1,8.9A1.88,1.88,0,0,1,.81,12.63H4.39A1.88,1.88,0,0,1,2.6,14.25ZM4.9,4.36a.44.44,0,0,1-.44.44A.44.44,0,0,1,4,4.36V2.14a.44.44,0,0,1,.44-.44.44.44,0,0,1,.44.44Zm1.91,0a.58.58,0,1,1-1.16,0V2.14a.7.7,0,0,0,0-.14.4.4,0,0,0,0-.15.58.58,0,1,1,1.16,0Zm.73-2.58a.43.43,0,0,0,0-.11.65.65,0,1,1,1.29,0V3H7.56V1.85S7.54,1.78,7.54,1.74ZM9.6,3V1.63s0,0,0-.06,0,0,0,0V1.29a.57.57,0,0,1,1.14,0V3.07a1.49,1.49,0,0,0-.36,0Zm4.58,8.86H10.77l2.59-6.53Zm-1.77,2.37a1.89,1.89,0,0,1-1.79-1.62H14.2A1.88,1.88,0,0,1,12.41,14.25Z"></path></svg>                            Racial Justice
                        </a>
</li>
<li class="topics-nav-item">
<a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/topics/healthcare">
<svg class="topic-icon" viewbox="0 0 15 15"><path d="M6.77,4.81H6.28a10,10,0,0,1-4.19-.92L.44,3a.67.67,0,0,1-.36-.67.7.7,0,0,1,.49-.6L4.38.65a1.14,1.14,0,0,1,1,.19,1.08,1.08,0,0,1,.42.86.56.56,0,0,0,.54.56h.48a.37.37,0,0,1,.37.37A.38.38,0,0,1,6.77,3H6.29A1.3,1.3,0,0,1,5,1.7a.31.31,0,0,0-.13-.26.31.31,0,0,0-.29-.06L.92,2.43l1.5.79a9.35,9.35,0,0,0,3.86.84h.49a.38.38,0,0,1,0,.75Z"></path><path d="M8.76,4.81H8.29a.38.38,0,1,1,0-.75h.47a9.24,9.24,0,0,0,3.87-.85l1.5-.78L10.45,1.38a.3.3,0,0,0-.27.05.35.35,0,0,0-.13.27A1.3,1.3,0,0,1,8.77,3H8.29a.38.38,0,0,1-.38-.38.37.37,0,0,1,.38-.37h.47A.56.56,0,0,0,9.3,1.7,1.1,1.1,0,0,1,9.72.84a1.08,1.08,0,0,1,.93-.19l3.82,1.1A.7.7,0,0,1,14.61,3L13,3.88A9.92,9.92,0,0,1,8.76,4.81Z"></path><path d="M7.52,13.82a.34.34,0,0,1-.23-.08L6,12.76a1.72,1.72,0,0,1-.61-1.52,1.63,1.63,0,0,1,.77-1.33L9.61,8A.88.88,0,0,0,10,7.33a1.11,1.11,0,0,0-.28-.91l-.33-.29a.38.38,0,1,1,.51-.55l.32.29a1.8,1.8,0,0,1,.52,1.55A1.59,1.59,0,0,1,10,8.65L6.56,10.56a.89.89,0,0,0-.39.74,1,1,0,0,0,.32.87l1.26,1a.39.39,0,0,1,.07.53A.41.41,0,0,1,7.52,13.82Z"></path><path d="M7.53,13.82a.41.41,0,0,1-.3-.14.39.39,0,0,1,.07-.53l1.26-1a1,1,0,0,0,.32-.87.89.89,0,0,0-.39-.74L5.07,8.65a1.62,1.62,0,0,1-.76-1.24,1.82,1.82,0,0,1,.51-1.54l.32-.29a.38.38,0,0,1,.51.55l-.33.3a1.08,1.08,0,0,0-.27.89A.92.92,0,0,0,5.44,8L8.86,9.91a1.63,1.63,0,0,1,.77,1.33A1.72,1.72,0,0,1,9,12.76l-1.26,1A.34.34,0,0,1,7.53,13.82Z"></path><path d="M7.52,15a.37.37,0,0,1-.37-.38V.44A.37.37,0,0,1,7.52.06.38.38,0,0,1,7.9.44V14.57A.38.38,0,0,1,7.52,15Z"></path></svg>                                    Health Care
                                </a>
</li>
<li class="topics-nav-item">
<a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/topics/criminal-justice">
<svg class="topic-icon" viewbox="0 0 15 15"><path d="M13.48,4h-.11A20.22,20.22,0,0,0,1.76,4a.38.38,0,0,1-.47-.26.37.37,0,0,1,.25-.46,21,21,0,0,1,12.05,0,.37.37,0,0,1-.11.73Z"></path><path d="M10.53,15h-6a.38.38,0,1,1,0-.75h6a.38.38,0,0,1,0,.75Z"></path><path d="M7.5,15a.37.37,0,0,1-.37-.38V.42A.37.37,0,0,1,7.5,0a.38.38,0,0,1,.38.38V14.6A.38.38,0,0,1,7.5,15Z"></path><path d="M5.39,9.89H.46a.37.37,0,0,1-.31-.16.36.36,0,0,1,0-.35L2.57,3.16a.38.38,0,0,1,.35-.24h0a.38.38,0,0,1,.35.23L5.74,9.38a.36.36,0,0,1,0,.35A.37.37,0,0,1,5.39,9.89ZM1,9.14H4.84L2.92,4.31Z"></path><path d="M14.6,9.89H9.67a.37.37,0,0,1-.31-.16.36.36,0,0,1,0-.35l2.46-6.22a.38.38,0,0,1,.35-.24h0a.37.37,0,0,1,.35.23L15,9.38a.36.36,0,0,1,0,.35A.37.37,0,0,1,14.6,9.89Zm-4.38-.75h3.83L12.13,4.31Z"></path><path d="M2.92,11.75A3,3,0,0,1,.1,9.62a.36.36,0,0,1,.06-.33.38.38,0,0,1,.3-.15H5.39a.38.38,0,0,1,.3.15.39.39,0,0,1,.06.33A3,3,0,0,1,2.92,11.75ZM1,9.89A2.21,2.21,0,0,0,2.92,11,2.22,2.22,0,0,0,4.83,9.89Z"></path><path d="M12.13,11.75A3,3,0,0,1,9.3,9.62a.39.39,0,0,1,.06-.33.38.38,0,0,1,.3-.15h4.93a.38.38,0,0,1,.3.15.36.36,0,0,1,.06.33A3,3,0,0,1,12.13,11.75ZM10.22,9.89a2.19,2.19,0,0,0,3.81,0Z"></path></svg>                                    Criminal Justice
                                </a>
</li>
<li class="topics-nav-item">
<a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/topics/technology">
<svg class="topic-icon" viewbox="0 0 15 15"><path d="M3.69,6.44a.38.38,0,0,1-.22-.07,2.46,2.46,0,0,1-1-2,2.42,2.42,0,0,1,4.78-.52A.39.39,0,0,1,7,4.32.39.39,0,0,1,6.5,4,1.67,1.67,0,1,0,3.91,5.76.37.37,0,0,1,4,6.28.4.4,0,0,1,3.69,6.44Z"></path><path d="M4,8.61H3.94A4.32,4.32,0,1,1,9.19,4.39a4.62,4.62,0,0,1,0,.65.37.37,0,0,1-.43.32.38.38,0,0,1-.31-.43,4.73,4.73,0,0,0,0-.54A3.57,3.57,0,1,0,4.11,7.87a.36.36,0,0,1,.28.44A.37.37,0,0,1,4,8.61Z"></path><path d="M10.67,14.91h-.06a.38.38,0,0,1-.24-.15L8,11.34,6.15,13.45a.38.38,0,0,1-.66-.19L3.94,3.72a.36.36,0,0,1,.15-.37.38.38,0,0,1,.4,0L13,8a.39.39,0,0,1,.19.35.38.38,0,0,1-.23.33l-2.59,1,2.4,3.39a.38.38,0,0,1-.09.53l-1.75,1.24A.41.41,0,0,1,10.67,14.91ZM8,10.36H8a.39.39,0,0,1,.29.16L10.76,14l1.14-.81L9.44,9.72a.33.33,0,0,1,0-.32.35.35,0,0,1,.22-.24l2.29-.91L4.8,4.36l1.3,8,1.61-1.86A.39.39,0,0,1,8,10.36Z"></path></svg>                                    Technology
                                </a>
</li>
<li class="topics-nav-item more"><a href="/topics/">More…</a></li>
</ul>
<!-- end .topics-nav -->
<ul class="extras-nav">
<li class="extras-nav-item series"><a href="/series/">Series</a></li>
<li class="extras-nav-item video"><a href="/video/">Video</a></li>
<li class="extras-nav-item impact"><a href="/impact/">Impact</a></li>
<li class="extras-nav-item search">
<a data-collapsible-target="#modal-search" href="/search">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-search"></use>
</svg>
<span class="cta">Search</span>
</a>
</li>
<li class="extras-nav-item more" nav="">
<a data-collapsible-target="#nav-menu" href="#footer-links">
<span class="cta">More</span>
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-nav-menu"></use>
</svg>
</a>
</li>
</ul>
<!-- end .extras-nav -->
</nav>
<!-- end .secondary-nav -->
</header>
<!-- end .masthead -->
</div>
</div>
<!-- end .masthead-wrap -->
<main class="content" id="main" role="main" tabindex="-1">
<div id="content-top"></div>
<div class="wrapper">
<div class="features" id="level1">
<div class="story-entry f1 region-national section-articles">
<h2 class="a11y">Featured Stories</h2>
<div class="lead-art">
<a class="aspect-3-2" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/dying-on-the-waitlist">
<img alt="" height="267" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%273%27%20height%3D%272%27%20style%3D%27background%3Atransparent%27%2F%3E" srcset="https://img.assets-d.propublica.org/v5/images/20210218-ecmo-la-opener.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.9416&amp;fp-y=0.2222&amp;h=267&amp;q=70&amp;w=400&amp;s=4d39079d5cbaa5b401c8018fbb213ddf 400w, https://img.assets-d.propublica.org/v5/images/20210218-ecmo-la-opener.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.9416&amp;fp-y=0.2222&amp;h=533&amp;q=80&amp;w=800&amp;s=de67f335b5779ddf826566bbc1e99a4b 800w, https://img.assets-d.propublica.org/v5/images/20210218-ecmo-la-opener.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.9416&amp;fp-y=0.2222&amp;h=800&amp;q=90&amp;w=1200&amp;s=1e06ce1d588d0f41844cc3eb88c3db42 1200w, https://img.assets-d.propublica.org/v5/images/20210218-ecmo-la-opener.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.9416&amp;fp-y=0.2222&amp;h=1067&amp;q=80&amp;w=1600&amp;s=9828728c6e975c6a7cab7589b55032ad 1600w" width="400"/>
</a>
</div>
<div class="description">
<div class="series-header">
<a class="series-title" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/series/coronavirus">Coronavirus</a>
</div>
<h3 class="hed"><a class="f1-link" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/dying-on-the-waitlist">Dying on the Waitlist</a></h3>
<div class="dek"><p>In Los Angeles County and around the country, doctors have had to decide who gets a lifesaving COVID-19 treatment and who doesn’t.</p>
</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/david-armstrong">David Armstrong</a> and <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/marshall-allen">Marshall Allen</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-18EST05:00">Feb. 18, 5 a.m. EST</time>
</p>
</div>
</div>
</div><!-- end .story-entry.f1 -->
<div class="story-entry f2 region-national section-articles">
<div class="lead-art">
<a class="aspect-3-2" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/the-irs-cashed-her-check-then-the-late-notices-started-coming">
<img alt="" height="267" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%273%27%20height%3D%272%27%20style%3D%27background%3Atransparent%27%2F%3E" srcset="https://img.assets-d.propublica.org/v5/images/20210211-irs-3x2-copy.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=267&amp;q=70&amp;w=400&amp;s=f7e72711aee4e173b1940e06db9ee248 400w, https://img.assets-d.propublica.org/v5/images/20210211-irs-3x2-copy.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=533&amp;q=80&amp;w=800&amp;s=32c863e1a04158f1ed9649eba87a3485 800w, https://img.assets-d.propublica.org/v5/images/20210211-irs-3x2-copy.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=800&amp;q=90&amp;w=1200&amp;s=30c53d13935cd37805c82a721470c327 1200w, https://img.assets-d.propublica.org/v5/images/20210211-irs-3x2-copy.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=1067&amp;q=80&amp;w=1600&amp;s=3ff50e83af2113aaa1c8f0827cb7f119 1600w" width="400"/>
</a>
</div>
<div class="description">
<h3 class="hed"><a class="f2-link" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/the-irs-cashed-her-check-then-the-late-notices-started-coming">The IRS Cashed Her Check. Then the Late Notices Started Coming.</a></h3>
<div class="dek"><p>A mountainous backlog of paperwork at the IRS continues to wreak havoc on America’s tax collection system — which especially hurts lower-income filers.</p>
</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/lydia-depillis">Lydia DePillis</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-16EST05:00">Feb. 16, 5 a.m. EST</time>
</p>
</div>
</div>
</div><!-- end .story-entry.f2 -->
<div class="features-group-f3-f4">
<div class="story-entry f3 region-national section-articles">
<div class="lead-art thumbnail">
<a class="f3-link" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/i-dont-trust-the-people-above-me-riot-squad-cops-open-up-about-disastrous-response-to-capitol-insurrection">
<picture>
<source media="(min-width: 48em)" sizes="(min-width: 1720px) 89px, (min-width: 960px) calc(4.86vw + 6px), (min-width: 768px) calc(7.5vw + 4px)" srcset="https://img.assets-d.propublica.org/v5/images/20210212-capitol-pd-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5469&amp;fp-y=0.4058&amp;h=75&amp;q=70&amp;w=75&amp;s=80ba69a6b54b4197d3c235282b9d471f 75w, https://img.assets-d.propublica.org/v5/images/20210212-capitol-pd-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5469&amp;fp-y=0.4058&amp;h=150&amp;q=70&amp;w=150&amp;s=7fc2f604fc288513942cf9d9ae1f33e0 150w, https://img.assets-d.propublica.org/v5/images/20210212-capitol-pd-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5469&amp;fp-y=0.4058&amp;h=400&amp;q=70&amp;w=400&amp;s=ffe65cbde5ad0fab22b758c7c29a376b 400w"/>
<source media="(max-width: 48em)" sizes="43.7vw" srcset="https://img.assets-d.propublica.org/v5/images/20210212-capitol-pd-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5469&amp;fp-y=0.4058&amp;h=267&amp;q=70&amp;w=400&amp;s=e205f850df6f788f121320a07e1aaf43 400w, https://img.assets-d.propublica.org/v5/images/20210212-capitol-pd-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5469&amp;fp-y=0.4058&amp;h=533&amp;q=80&amp;w=800&amp;s=778e08e273f5359be0d598f2f5968a8f 800w, https://img.assets-d.propublica.org/v5/images/20210212-capitol-pd-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5469&amp;fp-y=0.4058&amp;h=800&amp;q=90&amp;w=1200&amp;s=7ddd01578d80fdec99575a458ad8d136 1200w, https://img.assets-d.propublica.org/v5/images/20210212-capitol-pd-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5469&amp;fp-y=0.4058&amp;h=1067&amp;q=80&amp;w=1600&amp;s=ae370f26afb2f8989920534163020535 1600w"/>
<img alt="" src="https://assets-d.propublica.org/v5/images/20210212-capitol-pd-3x2.jpg"/>
</picture>
</a>
</div>
<div class="description">
<div class="series-header">
<a class="series-title" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/series/the-insurrection">The Insurrection</a>
</div>
<h3 class="hed"><a class="f3-link" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/i-dont-trust-the-people-above-me-riot-squad-cops-open-up-about-disastrous-response-to-capitol-insurrection">“I Don’t Trust the People Above Me”: Riot Squad Cops Open Up About Disastrous Response to Capitol Insurrection</a></h3></div></div></div></div></div></main></body></html>
<div class="dek"><p>Interviews with 19 current and former officers show how failures of leadership and communication put hundreds of Capitol cops at risk and allowed rioters to get dangerously close to members of Congress.</p>
</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/joaquin-sapien">Joaquin Sapien</a> and <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/joshua-kaplan">Joshua Kaplan</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-12EST13:30">Feb. 12, 1:30 p.m. EST</time>
</p>
</div>

<!-- end .story-entry.f3 -->
<div class="story-entry f4 region-national section-articles">
<div class="lead-art thumbnail">
<a class="f4-link" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/a-federal-appeals-court-has-ruled-in-favor-of-releasing-nypd-discipline-records">
<picture>
<source media="(min-width: 48em)" sizes="(min-width: 1720px) 89px, (min-width: 960px) calc(4.86vw + 6px), (min-width: 768px) calc(7.5vw + 4px)" srcset="https://img.assets-d.propublica.org/v5/images/20210217-nypd-court-case-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=75&amp;q=70&amp;w=75&amp;s=7643f6f541d47e28971d31fcf892657c 75w, https://img.assets-d.propublica.org/v5/images/20210217-nypd-court-case-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=150&amp;q=70&amp;w=150&amp;s=33c29dedb376d61f62565f1da8938dec 150w, https://img.assets-d.propublica.org/v5/images/20210217-nypd-court-case-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=400&amp;q=70&amp;w=400&amp;s=736d1867aa8cd4457e0e244a13b8dfc4 400w"/>
<source media="(max-width: 48em)" sizes="43.7vw" srcset="https://img.assets-d.propublica.org/v5/images/20210217-nypd-court-case-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=267&amp;q=70&amp;w=400&amp;s=bc00c83a70ebd4ad9b5a6d8f72224e67 400w, https://img.assets-d.propublica.org/v5/images/20210217-nypd-court-case-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=533&amp;q=80&amp;w=800&amp;s=adf69d309d063af2b343dc2d69e96ac0 800w, https://img.assets-d.propublica.org/v5/images/20210217-nypd-court-case-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=800&amp;q=90&amp;w=1200&amp;s=54331329e5b98be6592cfc0df8d2bbd7 1200w, https://img.assets-d.propublica.org/v5/images/20210217-nypd-court-case-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=1067&amp;q=80&amp;w=1600&amp;s=b7cfdc8caf8a7dd88a91b5253a2e1814 1600w"/>
<img alt="" src="https://assets-d.propublica.org/v5/images/20210217-nypd-court-case-3x2.jpg"/>
</picture>
</a>
</div>
<div class="description">
<div class="series-header">
<a class="series-title" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/series/the-nypd-files">The NYPD Files</a>
</div>
<h3 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/a-federal-appeals-court-has-ruled-in-favor-of-releasing-nypd-discipline-records">A Federal Appeals Court Has Ruled in Favor of Releasing NYPD Discipline Records</a></h3></div></div>
<div class="dek"><p>On Tuesday, the 2nd Circuit rejected unions’ appeal to keep NYPD discipline records secret. ProPublica published thousands of those files last year. “The cat is not only out of the bag, it’s running around the streets,” one judge noted then.</p>
</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/eric-umansky">Eric Umansky</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-17EST11:40">Feb. 17, 11:40 a.m. EST</time>
</p>
</div>

<!-- end .story-entry.f4 -->

<!-- end #level1.features -->
<div class="promo-covid" id="level1a">
<div class="pp-module" id="r6">
<div class="module-content" id="covid-module">
<div class="group-cover">
<a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/series/coronavirus"> <h2 class="pp-module-title">COVID-19</h2></a>
<div class="lead-art">
<a class="" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/series/coronavirus">
<img alt="" sizes="100vw" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%2730%27%20height%3D%2740%27%20style%3D%27background%3Atransparent%27%2F%3E" srcset="https://img.assets-c3.propublica.org/images/series/20200701-coronavirus-series-C.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=1067&amp;q=80&amp;w=800&amp;s=2eda8d818e708cae593da2712085f442 800w, https://img.assets-c3.propublica.org/images/series/20200701-coronavirus-series-C.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=800&amp;q=75&amp;w=600&amp;s=58b81b1531fbb12cd34309c063a6e793 600w, https://img.assets-c3.propublica.org/images/series/20200701-coronavirus-series-C.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=400&amp;q=65&amp;w=300&amp;s=7a71a2a4bd82d040d54b46f8b0d7ce27 300w"/>
</a>
</div> <!-- end .lead-art -->
</div><!-- end .group-header -->
<div class="group-features">
<h3 class="group-header">Featured Reporting on the Crisis</h3>
<div class="story-entry">
<div class="description">
<h3 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/nursing-homes-cuomo">Cuomo Still Underreporting the Total Count of COVID Nursing Home Deaths</a></h3>
<div class="dek"><p>The governor finally released data on nursing home cases after lawsuits and demands from lawmakers, but hundreds of presumed COVID-19 deaths have yet to be included in the state's official total.</p>
</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/joe-sexton">Joe Sexton</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-12EST15:30">Feb. 12, 3:30 p.m. EST</time>
</p>
</div>
</div>
</div><!-- end .story-entry.f1 -->
<div class="story-entry">
<div class="description">
<h3 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/people-over-75-are-first-in-line-to-be-vaccinated-against-covid-19-the-average-black-person-doesnt-live-that-long">People Over 75 Are First in Line to Be Vaccinated Against COVID-19. The Average Black Person Here Doesn’t Live That Long.</a></h3>
<div class="dek"><p>Prioritizing COVID-19 vaccinations for people 75 and up can leave out Black Americans, who tend to die younger than their white counterparts. In majority-Black Shelby County, this gap raises questions of how to make the vaccine rollout equitable.</p>
</div> <div class="metadata">
<p class="byline">by <span class="name">Wendi C. Thomas</span> and <span class="name">Hannah Grabenstein</span>,  <a href="https://mlk50.com/">MLK50: Justice Through Journalism</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-12EST11:30">Feb. 12, 11:30 a.m. EST</time>
</p>
</div>
</div>
</div><!-- end .story-entry.f1 -->
</div><!-- /end .covid-features -->
<div class="group-resources">
<h3 class="group-header">Resources</h3>
<div class="promo-in-brief">
<a class="brief" href="//projects.propub3r6espa33w.onion/coronavirus-contracts">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/2020-0527-contracts-app-500x500-pointer.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne75w/2020-0527-contracts-app-500x500-pointer.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/2020-0527-contracts-app-500x500-pointer.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne300w/2020-0527-contracts-app-500x500-pointer.jpg 300w">
<div class="brief-description">
<h3 class="brief-title">Coronavirus Contracts: Tracking Federal Purchases to Fight the Pandemic</h3>
</div><!-- /end .brief-description -->
</img></a>
</div><!-- /end .promo-in-brief -->
<div class="promo-in-brief">
<a class="brief" href="//projects.propub3r6espa33w.onion/coronavirus/bailouts/">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/20200707-ppp-look-up-app-1x1-b.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne75w/20200707-ppp-look-up-app-1x1-b.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/20200707-ppp-look-up-app-1x1-b.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne300w/20200707-ppp-look-up-app-1x1-b.jpg 300w"/>
<div class="brief-description">
<h3 class="brief-title">Tracking PPP Loans: Search Every Company Approved for Federal Loans Over $150k</h3>
</div><!-- /end .brief-description -->
</a>
</div><!-- /end .promo-in-brief -->
<div class="promo-in-brief">
<a class="brief" href="//projects.propub3r6espa33w.onion/coronavirus-unemployment">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/20200720-unemployment-1x1-b.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/externals/_oneOne75w/20200720-unemployment-1x1-b.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne150w/20200720-unemployment-1x1-b.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/externals/_oneOne300w/20200720-unemployment-1x1-b.jpg 300w"/>
<div class="brief-description">
<h3 class="brief-title">What Coronavirus Job Losses Reveal About Racism in America</h3>
</div><!-- /end .brief-description -->
</a>
</div><!-- /end .promo-in-brief -->
</div><!-- /end .covid-resources -->
</div><!-- /end .module-content -->
</div><!-- /end .pp-module -->
</div>
<div class="evergreen" id="level1b">
<div class="pp-module" id="featured-series-module">
<div class="module-content" id="featured-series">
<h2 class="pp-module-title">The Effort to Overturn the Election</h2>
<a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/series/the-insurrection">
<img alt="" class="series-img" sizes="100vw" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%2730%27%20height%3D%2740%27%20style%3D%27background%3Atransparent%27%2F%3E" srcset="https://img.assets-d.propublica.org/v5/images/20210113-insurrection-promo.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=1067&amp;q=80&amp;w=800&amp;s=109bd88547266597a32ae820aa2bea1c 800w, https://img.assets-d.propublica.org/v5/images/20210113-insurrection-promo.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=800&amp;q=75&amp;w=600&amp;s=e530f4df25f59078121b2d381582ec04 600w, https://img.assets-d.propublica.org/v5/images/20210113-insurrection-promo.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=400&amp;q=65&amp;w=300&amp;s=1c820505675eb71cd5060c3f0e3d1a2b 300w"/>
</a>
<div class="series-detail">
<p>Reporting on the mob that attacked and breached the Capitol, the fallout from that day, and ongoing far-right violence.</p>
</div>
</div>
</div>
<div class="pp-module promo-in-brief" id="featured-reporting-module">
<div class="module-content" id="featured-reporting">
<h2 class="pp-module-title">Featured Reporting</h2>
<a class="brief has-photo" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/solarwinds-cybersecurity-system">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210202-solarwinds-hack-in-toto_500x500.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210202-solarwinds-hack-in-toto_500x500.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210202-solarwinds-hack-in-toto_500x500.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210202-solarwinds-hack-in-toto_500x500.jpg 300w">
<div class="brief-description" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/solarwinds-cybersecurity-system">
<h3 class="brief-title">The U.S. Spent $2.2 Million on a Cybersecurity System That Wasn’t Implemented — and Might Have Stopped a Major Hack</h3>
</div><!-- /end .brief-description -->
</img></a>
<a class="brief has-photo" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/new-york-city-council-proposes-sweeping-nypd-reforms">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210201-nypd-reform-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210201-nypd-reform-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210201-nypd-reform-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210201-nypd-reform-1x1.jpg 300w"/>
<div class="brief-description" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/new-york-city-council-proposes-sweeping-nypd-reforms">
<h3 class="brief-title">New York City Council Proposes Sweeping NYPD Reforms</h3>
</div><!-- /end .brief-description -->
</a>
<a class="brief has-photo" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/boogaloo-bois-military-training">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210201-boog-military-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210201-boog-military-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210201-boog-military-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210201-boog-military-1x1.jpg 300w"/>
<div class="brief-description" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/boogaloo-bois-military-training">
<h3 class="brief-title">The Boogaloo Bois Have Guns, Criminal Records and Military Training. Now They Want to Overthrow the Government.</h3>
</div><!-- /end .brief-description -->
</a>
</div><!-- /end .module-content -->
</div>
</div>
<div class="river" id="level2">
<div class="story-entry region-national section-atPropublica">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/atpropublica/richard-tofel-to-retire-as-propublica-president-board-launches-search-for-successor">Richard Tofel to Retire as ProPublica President; Board Launches Search for Successor</a></h2>
<div class="metadata">
<p class="byline"><a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-17EST12:00">Feb. 17, 12 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-atPropublica">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/atpropublica/propublicas-nypd-files-wins-john-jay-college-harry-frank-guggenheim-award-for-excellence-in-criminal-justice-journalism">ProPublica’s “NYPD Files” Wins John Jay College/Harry Frank Guggenheim Award for Excellence in Criminal Justice Reporting</a></h2>
<div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-11EST15:16">Feb. 11, 3:16 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/fauci-vaccines-kids">
<img alt="" height="75" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%271%27%20height%3D%271%27%20style%3D%27background%3Atransparent%27%2F%3E" srcset="https://img.assets-d.propublica.org/v5/images/20210211-children-vaccine-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4764&amp;fp-y=0.4096&amp;h=75&amp;q=70&amp;w=75&amp;s=9c999c5d4aa6d85788d6b6cfdbbbbc6c 75w, https://img.assets-d.propublica.org/v5/images/20210211-children-vaccine-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4764&amp;fp-y=0.4096&amp;h=150&amp;q=70&amp;w=150&amp;s=5d603c0dca97f93efc19760d02409543 150w, https://img.assets-d.propublica.org/v5/images/20210211-children-vaccine-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4764&amp;fp-y=0.4096&amp;h=400&amp;q=70&amp;w=400&amp;s=b825ec6b6eaac28303a30fc6f1cf9ee5 400w" width="75"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/fauci-vaccines-kids">Fauci: Vaccines for Kids as Young as First Graders Could Be Authorized by September</a></h2>
<div class="dek"><p>For this to happen by the start of the next school year, trials need to prove the vaccine is safe and effective in children. Experts say manufacturers aren’t moving quickly enough.</p>
</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/caroline-chen">Caroline Chen</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-11EST05:00">Feb. 11, 5 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/utility-companies-owe-millions-to-this-state-regulatory-agency-the-problem-the-agency-cant-track-what-its-owed">
<img alt="" height="75" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%271%27%20height%3D%271%27%20style%3D%27background%3Atransparent%27%2F%3E" srcset="https://img.assets-d.propublica.org/v5/images/20210210-cpuc-folo-money-stebbins-hands.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4661&amp;fp-y=0.4299&amp;h=75&amp;q=70&amp;w=75&amp;s=f42a1e0ab6e2d363060816282d056160 75w, https://img.assets-d.propublica.org/v5/images/20210210-cpuc-folo-money-stebbins-hands.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4661&amp;fp-y=0.4299&amp;h=150&amp;q=70&amp;w=150&amp;s=143ebb34b7754aaa73a31bbc8b00840e 150w, https://img.assets-d.propublica.org/v5/images/20210210-cpuc-folo-money-stebbins-hands.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.4661&amp;fp-y=0.4299&amp;h=400&amp;q=70&amp;w=400&amp;s=44e8f3f280110f546d7f4fc199ba2ef8 400w" width="75"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/utility-companies-owe-millions-to-this-state-regulatory-agency-the-problem-the-agency-cant-track-what-its-owed">Utility Companies Owe Millions to This State Regulatory Agency. The Problem? The Agency Can’t Track What It’s Owed.</a></h2>
<div class="dek">When a whistleblower alleged that $200 million was missing from the California Public Utilities Commission, the agency says it took steps to collect. Yet an audit uncovered more missing money and cited flaws in the agency’s accounting system.</div> <div class="metadata">
<p class="byline">by <span class="name">Scott Morris</span>,  <a href="http://www.baycitynews.org/">Bay City News Foundation</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-10EST14:00">Feb. 10, 2 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-atPropublica">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/atpropublica/megan-omatz-joins-propublicas-midwest-newsroom">Megan O’Matz Joins ProPublica’s Midwest Newsroom</a></h2>
<div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-10EST12:30">Feb. 10, 12:30 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/nsu-section-230">
<img alt="" height="75" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%271%27%20height%3D%271%27%20style%3D%27background%3Atransparent%27%2F%3E" srcset="https://img.assets-d.propublica.org/v5/images/20210209-nsu-section-230-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=75&amp;q=70&amp;w=75&amp;s=a7355a4865304a8029a4c14a5a6446aa 75w, https://img.assets-d.propublica.org/v5/images/20210209-nsu-section-230-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=150&amp;q=70&amp;w=150&amp;s=37cf6f9f8602eada0044fd112cf76b3b 150w, https://img.assets-d.propublica.org/v5/images/20210209-nsu-section-230-3x2.jpg?crop=focalpoint&amp;fit=crop&amp;fp-x=0.5&amp;fp-y=0.5&amp;h=400&amp;q=70&amp;w=400&amp;s=310db174fef3d8a06adb7549229248e2 400w" width="75"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/nsu-section-230">Twenty-Six Words Created the Internet. What Will It Take to Save It?</a></h2>
<div class="dek">Jeff Kosseff wrote the book on Section 230, the law that gave us the internet we have today. He talks with ProPublica Editor-in-Chief Stephen Engelberg about how we got here and how we should regulate our way out.</div> <div class="metadata">
<p class="byline"><a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/stephen-engelberg">Stephen Engelberg</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-09EST14:00">Feb. 9, 2 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-atPropublica">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/atpropublica/two-propublica-local-reporting-network-projects-named-finalists-for-shadid-award-for-journalism-ethics">Two ProPublica Local Reporting Network Projects Named Finalists for Shadid Award for Journalism Ethics</a></h2>
<div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-08EST15:29">Feb. 8, 3:29 p.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-local-reporting-network section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/how-we-found-pricey-provisions-in-new-jersey-police-contracts">
<img alt="" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210208-nj-contracts-methodology-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210208-nj-contracts-methodology-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210208-nj-contracts-methodology-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210208-nj-contracts-methodology-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/how-we-found-pricey-provisions-in-new-jersey-police-contracts">How We Found Pricey Provisions in New Jersey Police Contracts</a></h2>
<div class="dek">ProPublica and the Asbury Park Press scoured hundreds of police union agreements for details on publicly funded payouts to cops.</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/agnes-chang">Agnes Chang</a>, <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/jeff-kao">Jeff Kao</a> and <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/agnel-philip">Agnel Philip</a>, ProPublica, and <span class="name">Andrew Ford</span>,  <a href="https://www.app.com/">Asbury Park Press</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-08EST05:01">Feb. 8, 5:01 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-local-reporting-network section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/new-jersey-police-contracts">
<img alt="" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210208-new-jersey-union-contracts-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210208-new-jersey-union-contracts-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210208-new-jersey-union-contracts-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210208-new-jersey-union-contracts-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/new-jersey-police-contracts">How the Police Bank Millions Through Their Union Contracts</a></h2>
<div class="dek">The public funds six-figure “sick day” payouts, $2,500 “perfect attendance” bonuses and lucrative “extra duty” assignments identified in a ProPublica, Asbury Park Press analysis of New Jersey police union contracts.</div> <div class="metadata">
<p class="byline">by <span class="name">Andrew Ford</span>,  <a href="https://www.app.com/">Asbury Park Press</a>, and <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/agnes-chang">Agnes Chang</a>, <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/jeff-kao">Jeff Kao</a> and <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/agnel-philip">Agnel Philip</a>, ProPublica</p> <p class="timestamp"><time class="timestamp" datetime="2021-02-08EST05:00">Feb. 8, 5 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-local-reporting-network section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/hawaii-beaches-legislation">
<img alt="" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210206-hawaii-sandbags-shore-impact-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210206-hawaii-sandbags-shore-impact-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210206-hawaii-sandbags-shore-impact-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210206-hawaii-sandbags-shore-impact-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/hawaii-beaches-legislation">Hawaii’s Beaches Are Disappearing. New Legislation Could Help ... if It’s Enforced.</a></h2>
<div class="dek">A legal loophole allowed wealthy property owners to protect their real estate at the expense of Hawaii’s coastlines. Now, the state Legislature is considering bills to crack down on the destructive practices, but questions around enforcement remain.</div> <div class="metadata">
<p class="byline">by <span class="name">Sophie Cocke</span>,  <a href="https://www.staradvertiser.com/">Honolulu Star-Advertiser</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-07EST05:01">Feb. 7, 5:01 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/why-opening-restaurants-is-exactly-what-the-coronavirus-wants-us-to-do">
<img alt="" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210206-variants-reopening-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210206-variants-reopening-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210206-variants-reopening-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210206-variants-reopening-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/why-opening-restaurants-is-exactly-what-the-coronavirus-wants-us-to-do">Why Opening Restaurants Is Exactly What the Coronavirus Wants Us to Do</a></h2>
<div class="dek">Governors continue to open indoor dining and other activities before vaccinations become widespread. Experts warn this could create superspreading playgrounds for dangerous variants and squander our best shot at getting the pandemic under control.</div> <div class="metadata">
<p class="byline"><a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/caroline-chen">Caroline Chen</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-06EST05:00">Feb. 6, 5 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-local-reporting-network section-articles">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/propublica-is-seeking-new-applicants-for-its-local-reporting-network">ProPublica Is Seeking New Applicants for Its Local Reporting Network</a></h2>
<div class="dek">We are looking to work with three more newsrooms, for a year beginning in April 2021, on accountability journalism projects.</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-05EST11:07">Feb. 5, 11:07 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-atPropublica">
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/atpropublica/propublica-hires-jenny-deam-to-cover-health-care">ProPublica Hires Jenny Deam to Cover Health Care</a></h2>
<div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/propublica">ProPublica</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-05EST11:00">Feb. 5, 11 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/we-have-counties-in-deep-trouble-oregon-lawmakers-seek-to-reverse-timber-tax-cuts-that-cost-communities-billions">
<img alt="" src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210205-oregon-timber-impact-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210205-oregon-timber-impact-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210205-oregon-timber-impact-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210205-oregon-timber-impact-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/we-have-counties-in-deep-trouble-oregon-lawmakers-seek-to-reverse-timber-tax-cuts-that-cost-communities-billions">“We Have Counties in Deep Trouble”: Oregon Lawmakers Seek to Reverse Timber Tax Cuts That Cost Communities Billions</a></h2>
<div class="dek">For decades, corporate timber benefited from tax cuts that devastated local budgets. Lawmakers want change and have filed dozens of bills, making this one of Oregon’s most consequential sessions for forest policy.</div> <div class="metadata">
<p class="byline">by <span class="name">Rob Davis</span>,  <a href="https://www.oregonlive.com/">The Oregonian/OregonLive</a>, and <span class="name">Tony Schick</span>,  Oregon Public Broadcasting</p> <p class="timestamp"><time class="timestamp" datetime="2021-02-05EST09:00">Feb. 5, 9 a.m. EST</time>
</p>
</div>
</div>
</div>
<div class="story-entry region-national section-articles">
<div class="lead-art thumbnail">
<a class="aspect-1-1" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/rich-investors-stripped-millions-from-a-hospital-chain-and-want-to-leave-it-behind-a-tiny-state-stands-in-their-way">
<img alt="An illustration of a hand taking apart a hospital." src="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210204-private-equity-illo-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/articles/_oneOne75w/20210204-private-equity-illo-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne150w/20210204-private-equity-illo-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/articles/_oneOne300w/20210204-private-equity-illo-1x1.jpg 300w"/>
</a>
</div>
<div class="description">
<h2 class="hed"><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/rich-investors-stripped-millions-from-a-hospital-chain-and-want-to-leave-it-behind-a-tiny-state-stands-in-their-way">Rich Investors Stripped Millions From a Hospital Chain and Want to Leave It Behind. A Tiny State Stands in Their Way.</a></h2>
<div class="dek">Private equity firm Leonard Green and other investors extracted $645 million from Prospect Medical before announcing a deal to sell it and leave it with $1.3 billion in financial obligations. Four states approved it — but Rhode Island is holding out.</div> <div class="metadata">
<p class="byline">by <a class="name" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/people/peter-elkind">Peter Elkind</a></p> <p class="timestamp"><time class="timestamp" datetime="2021-02-04EST13:22">Feb. 4, 1:22 p.m. EST</time>
</p>
</div>
</div>
</div>
<div id="content-bottom"></div>
<footer class="browse-by-month">
<form>
<nav>
<label for="browse-archive">Browse the archive:</label>
<select class="menu-jump" id="browse-archive" name="browse-archive">
<option value="">Choose a month</option>
<option value="/archive/2021/02">February 2021</option>
<option value="/archive/2021/01">January 2021</option>
<option value="/archive/2020/12">December 2020</option>
<option value="/archive/2020/11">November 2020</option>
<option value="/archive/2020/10">October 2020</option>
<option value="/archive/2020/09">September 2020</option>
<option value="/archive/2020/08">August 2020</option>
<option value="/archive/2020/07">July 2020</option>
<option value="/archive/2020/06">June 2020</option>
<option value="/archive/2020/05">May 2020</option>
<option value="/archive/2020/04">April 2020</option>
<option value="/archive/2020/03">March 2020</option>
<option value="/archive/2020/02">February 2020</option>
<option value="/archive/2020/01">January 2020</option>
<option value="/archive/2019/12">December 2019</option>
<option value="/archive/2019/11">November 2019</option>
<option value="/archive/2019/10">October 2019</option>
<option value="/archive/2019/09">September 2019</option>
<option value="/archive/2019/08">August 2019</option>
<option value="/archive/2019/07">July 2019</option>
<option value="/archive/2019/06">June 2019</option>
<option value="/archive/2019/05">May 2019</option>
<option value="/archive/2019/04">April 2019</option>
<option value="/archive/2019/03">March 2019</option>
<option value="/archive/2019/02">February 2019</option>
<option value="/archive/2019/01">January 2019</option>
<option value="/archive/2018/12">December 2018</option>
<option value="/archive/2018/11">November 2018</option>
<option value="/archive/2018/10">October 2018</option>
<option value="/archive/2018/09">September 2018</option>
<option value="/archive/2018/08">August 2018</option>
<option value="/archive/2018/07">July 2018</option>
<option value="/archive/2018/06">June 2018</option>
<option value="/archive/2018/05">May 2018</option>
<option value="/archive/2018/04">April 2018</option>
<option value="/archive/2018/03">March 2018</option>
<option value="/archive/2018/02">February 2018</option>
<option value="/archive/2018/01">January 2018</option>
<option value="/archive/2017/12">December 2017</option>
<option value="/archive/2017/11">November 2017</option>
<option value="/archive/2017/10">October 2017</option>
<option value="/archive/2017/09">September 2017</option>
<option value="/archive/2017/08">August 2017</option>
<option value="/archive/2017/07">July 2017</option>
<option value="/archive/2017/06">June 2017</option>
<option value="/archive/2017/05">May 2017</option>
<option value="/archive/2017/04">April 2017</option>
<option value="/archive/2017/03">March 2017</option>
<option value="/archive/2017/02">February 2017</option>
<option value="/archive/2017/01">January 2017</option>
<option value="/archive/2016/12">December 2016</option>
<option value="/archive/2016/11">November 2016</option>
<option value="/archive/2016/10">October 2016</option>
<option value="/archive/2016/09">September 2016</option>
<option value="/archive/2016/08">August 2016</option>
<option value="/archive/2016/07">July 2016</option>
<option value="/archive/2016/06">June 2016</option>
<option value="/archive/2016/05">May 2016</option>
<option value="/archive/2016/04">April 2016</option>
<option value="/archive/2016/03">March 2016</option>
<option value="/archive/2016/02">February 2016</option>
<option value="/archive/2016/01">January 2016</option>
<option value="/archive/2015/12">December 2015</option>
<option value="/archive/2015/11">November 2015</option>
<option value="/archive/2015/10">October 2015</option>
<option value="/archive/2015/09">September 2015</option>
<option value="/archive/2015/08">August 2015</option>
<option value="/archive/2015/07">July 2015</option>
<option value="/archive/2015/06">June 2015</option>
<option value="/archive/2015/05">May 2015</option>
<option value="/archive/2015/04">April 2015</option>
<option value="/archive/2015/03">March 2015</option>
<option value="/archive/2015/02">February 2015</option>
<option value="/archive/2015/01">January 2015</option>
<option value="/archive/2014/12">December 2014</option>
<option value="/archive/2014/11">November 2014</option>
<option value="/archive/2014/10">October 2014</option>
<option value="/archive/2014/09">September 2014</option>
<option value="/archive/2014/08">August 2014</option>
<option value="/archive/2014/07">July 2014</option>
<option value="/archive/2014/06">June 2014</option>
<option value="/archive/2014/05">May 2014</option>
<option value="/archive/2014/04">April 2014</option>
<option value="/archive/2014/03">March 2014</option>
<option value="/archive/2014/02">February 2014</option>
<option value="/archive/2014/01">January 2014</option>
<option value="/archive/2013/12">December 2013</option>
<option value="/archive/2013/11">November 2013</option>
<option value="/archive/2013/10">October 2013</option>
<option value="/archive/2013/09">September 2013</option>
<option value="/archive/2013/08">August 2013</option>
<option value="/archive/2013/07">July 2013</option>
<option value="/archive/2013/06">June 2013</option>
<option value="/archive/2013/05">May 2013</option>
<option value="/archive/2013/04">April 2013</option>
<option value="/archive/2013/03">March 2013</option>
<option value="/archive/2013/02">February 2013</option>
<option value="/archive/2013/01">January 2013</option>
<option value="/archive/2012/12">December 2012</option>
<option value="/archive/2012/11">November 2012</option>
<option value="/archive/2012/10">October 2012</option>
<option value="/archive/2012/09">September 2012</option>
<option value="/archive/2012/08">August 2012</option>
<option value="/archive/2012/07">July 2012</option>
<option value="/archive/2012/06">June 2012</option>
<option value="/archive/2012/05">May 2012</option>
<option value="/archive/2012/04">April 2012</option>
<option value="/archive/2012/03">March 2012</option>
<option value="/archive/2012/02">February 2012</option>
<option value="/archive/2012/01">January 2012</option>
<option value="/archive/2011/12">December 2011</option>
<option value="/archive/2011/11">November 2011</option>
<option value="/archive/2011/10">October 2011</option>
<option value="/archive/2011/09">September 2011</option>
<option value="/archive/2011/08">August 2011</option>
<option value="/archive/2011/07">July 2011</option>
<option value="/archive/2011/06">June 2011</option>
<option value="/archive/2011/05">May 2011</option>
<option value="/archive/2011/04">April 2011</option>
<option value="/archive/2011/03">March 2011</option>
<option value="/archive/2011/02">February 2011</option>
<option value="/archive/2011/01">January 2011</option>
<option value="/archive/2010/12">December 2010</option>
<option value="/archive/2010/11">November 2010</option>
<option value="/archive/2010/10">October 2010</option>
<option value="/archive/2010/09">September 2010</option>
<option value="/archive/2010/08">August 2010</option>
<option value="/archive/2010/07">July 2010</option>
<option value="/archive/2010/06">June 2010</option>
<option value="/archive/2010/05">May 2010</option>
<option value="/archive/2010/04">April 2010</option>
<option value="/archive/2010/03">March 2010</option>
<option value="/archive/2010/02">February 2010</option>
<option value="/archive/2010/01">January 2010</option>
<option value="/archive/2009/12">December 2009</option>
<option value="/archive/2009/11">November 2009</option>
<option value="/archive/2009/10">October 2009</option>
<option value="/archive/2009/09">September 2009</option>
<option value="/archive/2009/08">August 2009</option>
<option value="/archive/2009/07">July 2009</option>
<option value="/archive/2009/06">June 2009</option>
<option value="/archive/2009/05">May 2009</option>
<option value="/archive/2009/04">April 2009</option>
<option value="/archive/2009/03">March 2009</option>
<option value="/archive/2009/02">February 2009</option>
<option value="/archive/2009/01">January 2009</option>
<option value="/archive/2008/12">December 2008</option>
<option value="/archive/2008/11">November 2008</option>
<option value="/archive/2008/10">October 2008</option>
<option value="/archive/2008/09">September 2008</option>
<option value="/archive/2008/08">August 2008</option>
<option value="/archive/2008/07">July 2008</option>
<option value="/archive/2008/06">June 2008</option>
<option value="/archive/2008/05">May 2008</option>
<option value="/archive/2008/04">April 2008</option>
<option value="/archive/2008/03">March 2008</option>
<option value="/archive/2008/02">February 2008</option>
<option value="/archive/2008/01">January 2008</option>
<option value="/archive/2007/12">December 2007</option>
<option value="/archive/2007/11">November 2007</option>
<option value="/archive/2007/10">October 2007</option>
</select>
</nav></form>
</footer></div>

<!-- end #level2.river -->
<div class="promos-1" id="level3">
<div class="pp-module module-follow" id="l1">
<h2 class="pp-module-title">Follow ProPublica</h2>
<ul class="list-plain links-social">
<li>
<a class="btn btn-social social-main social-link-twitter" href="https://twitter.com/propublica" id="twitter-follow-pp-module">
<svg class="icon" height="24" width="24"><use xlink:href="#icon-twitter"></use></svg>
<span>Twitter</span>
</a>
</li>
<li>
<a class="btn btn-social social-main social-link-fb" href="https://www.facebookcorewwwi.onion/propublica/" id="facebook-follow-pp-module">
<svg class="icon" height="24" width="24"><use xlink:href="#icon-facebook"></use></svg>
<span>Facebook</span>
</a>
</li>
<li>
<a class="btn btn-social social-main social-link-youtube" href="https://www.youtube.com/user/propublica" id="youtube-follow-pp-module">
<svg class="icon" height="24" width="24"><use xlink:href="#icon-youtube"></use></svg>
<span>YouTube</span>
</a>
</li>
<li>
<a class="btn btn-social social-main social-link-rss" href="//feeds.propub3r6espa33w.onion/propublica/main" id="rss-follow-pp-module">
<svg class="icon" height="24" width="24"><use xlink:href="#icon-rss"></use></svg>
<span><abbr title="Rich Site Summary">RSS</abbr></span>
</a>
</li>
</ul>
<form action="https://signup.propublica.org/newsletter/turing" class="ajax-form form-subscribe" id="sidebar-email-subscribe-module" method="post">
<h2 class="pp-module-title">Stay Informed</h2>
<p class="pp-module-description">Get our investigations delivered to your inbox with the Big Story newsletter.</p>
<div class="form-label-set">
<input class="input-text" id="sidebar-email-subscribe-input" name="email" onfocus="recaptchaInit()" placeholder="Enter your email" required="" type="email"/>
<label for="sidebar-email-subscribe-input">
<span class="a11y">Email address</span>
<svg aria-hidden="true" class="icon"><use xlink:href="#icon-email"></use></svg>
</label>
</div>
<input class="g-recaptcha" id="newsletter-signup-follow-main" type="submit" value="Sign Up"/>
<input name="success_location" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/forms/thankyou"/>
<input name="error_location" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/forms/error"/>
<input name="target" type="hidden" value="l/125411/2018-11-01/5vd2q2"/>
<input name="origin_location" type="hidden" value="https://v3-www.propublica.org/"/>
<small class="legal">This site is protected by reCAPTCHA and the Google <a href="https://policies.google.com/privacy">Privacy Policy</a> and <a href="https://policies.google.com/terms">Terms of Service</a> apply.</small>
</form>
</div><!-- end .pp-module.module-follow -->
<a class="pp-module module-donate has-photo" href="https://donate.propublica.org/" id="donate-module">
<div class="module-photo is-bg" style="background-image: url('//assets.propub3r6espa33w.onion/static/prod/v4/images/donate-photo-a.png');"></div>
<div class="module-content donate-content">
<h2 class="module-hed hed-donate">It’s not too late to <strong>Vote ProPublica</strong></h2>
<span class="btn btn-urgent">Donate</span>
</div><!-- /end .module-content -->
</a><!-- /end .pp-module.module-donate.has-bg -->
<div class="pp-module module-awards has-photo" id="l3">
<p class="module-photo">
<img alt="" src="//assets.propub3r6espa33w.onion/static/prod/v4/images/medal_franklin_128.png"/>
</p>
<div class="module-content">
<h2 class="pp-module-title">Awards</h2>
<p>ProPublica has been a recipient of the Pulitzer Prizes for public service, explanatory reporting, national reporting, investigative reporting and feature writing. <a href="/awards" id="awards-module">See the full list of our awards</a>.</p>
</div><!-- end .module-content -->
</div><!-- end .pp-module.module-awards -->
<div class="pp-module module-corrections" id="l4">
<div class="module-content">
<h2 class="pp-module-title">Complaints &amp; Corrections</h2>
<p>To contact us with concerns and corrections, <a href="/cdn-cgi/l/email-protection#5b3d3e3e3f393a38301b2b29342b2e393732383a7534293c">email us</a>. All emails may be published unless you tell us otherwise. <a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/corrections" id="corrections-module">Read our corrections</a>.</p>
</div><!-- end .module-content -->
</div><!-- end .pp-module.module-corrections -->
</div><!-- end #level3.promos-1 -->
<div class="promos-2" id="level4">
<!-- /2219821/Desktop_MedRec_1 -->
<div class="ad ad-article ad-300x250">
<div class="inner htl-ad" data-eager="" data-prebid="0x0:|960x0:Desktop_Medrec_1" data-refresh="viewable" data-refresh-secs="60" data-sizes="0x0:|960x0:1x1,300x250,300x600" data-unit="Desktop_Medrec_1"></div>
</div><!-- end .ad -->
<div class="site-promo-module" id="r3">
<a href="/nerds" id="nerd-blog-promo">
<img alt="" src="//assets.propub3r6espa33w.onion/static/prod/v4/images/promo-nerd-blog.gif"/>
<div class="site-text">
<h2 class="site-title">ProPublica Nerd Blog</h2>
<p class="site-description">Secrets for data journalists and newsroom developers
                        </p>
</div>
</a>
</div>
<div class="site-promo-module" id="r4">
<a href="/datastore" id="datastore-promo">
<img alt="" src="//assets.propub3r6espa33w.onion/static/prod/v4/images/promo-data-store.gif"/>
<div class="site-text">
<h2 class="site-title">ProPublica Data Store</h2>
<p class="site-description">Download or purchase the data behind our journalism</p>
</div>
</a>
</div>
<div class="pp-module promo-in-brief" id="r5">
<div class="module-content">
<a class="brief has-photo" href="/series/trump-inc" id="podcast-promo">
<img alt="" class="brief-thumb" src="//assets.propub3r6espa33w.onion/static/prod/v4/images/promo-podcast-trump-inc.jpg"/>
<div class="brief-description">
<h2 class="hed">Podcast: Trump, Inc.</h2>
<p class="brief-title">Exploring the mysteries of the president’s businesses, who profits and at what cost.</p>
</div>
</a>
</div>
</div>
<!-- end .pp-module.promo-in-brief -->
<div class="pp-module promo-in-brief" id="r6">
<div class="module-content" id="get-involved-module">
<h2 class="pp-module-title">Get Involved</h2>
<a class="brief" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/getinvolved/juvenile-justice-fees">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/2021-0128-juvenile-justice-folo-500x500.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne75w/2021-0128-juvenile-justice-folo-500x500.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/2021-0128-juvenile-justice-folo-500x500.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne300w/2021-0128-juvenile-justice-folo-500x500.jpg 300w"/>
<div class="brief-description">
<h3 class="brief-title">Has Your Family Paid Fees or Fines to a Juvenile Justice System?</h3>
</div><!-- /end .brief-description -->
</a>
<a class="brief" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/getinvolved/help-propublica-find-the-most-important-stories-of-2021">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/20210113-pitch-propublica-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne75w/20210113-pitch-propublica-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/20210113-pitch-propublica-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne300w/20210113-pitch-propublica-1x1.jpg 300w"/>
<div class="brief-description">
<h3 class="brief-title">Help ProPublica Find the Most Important Stories of 2021</h3>
</div><!-- /end .brief-description -->
</a>
<a class="brief" href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/getinvolved/did-you-attend-the-milton-hershey-school-were-investigating-it-help-us">
<img alt="" class="brief-thumb" src="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/20201123-hershey-callout-1x1.jpg" srcset="//assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne75w/20201123-hershey-callout-1x1.jpg 75w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne150w/20201123-hershey-callout-1x1.jpg 150w, //assets-c3.propub3r6espa33w.onion/images/getInvolved/_oneOne300w/20201123-hershey-callout-1x1.jpg 300w"/>
<div class="brief-description">
<h3 class="brief-title">Did You Attend the Milton Hershey School? We’re Reporting on It. Help Us.</h3>
</div><!-- /end .brief-description -->
</a>
</div><!-- /end .module-content -->
</div><!-- /end .pp-module.promo-in-brief -->
<div class="pp-module module-popular" id="r7">
<h2 class="pp-module-title">Most Popular Stories</h2>
<div class="module-content" id="hottest-stories-module">
<div class="tabs">
<div class="pane-stories collapsible" data-collapsible-set="pp-stories-hottest">
<h3 class="hed-stories">Most Read</h3>
<ul class="list-plain list-stories">
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/dying-on-the-waitlist" title="View this">Dying on the Waitlist</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/i-dont-trust-the-people-above-me-riot-squad-cops-open-up-about-disastrous-response-to-capitol-insurrection" title="View this">“I Don’t Trust the People Above Me”: Riot Squad Cops Open Up About Disastrous Response to Capitol Insurrection</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/turbotax-just-tricked-you-into-paying-to-file-your-taxes" title="View this">Here’s How TurboTax Just Tricked You Into Paying to File Your Taxes</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/the-irs-cashed-her-check-then-the-late-notices-started-coming" title="View this">The IRS Cashed Her Check. Then the Late Notices Started Coming.</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/people-over-75-are-first-in-line-to-be-vaccinated-against-covid-19-the-average-black-person-doesnt-live-that-long" title="View this">People Over 75 Are First in Line to Be Vaccinated Against COVID-19. The Average Black Person Here Doesn’t Live That Long.</a></li>
</ul>
</div><!-- /end .pane-stories -->
<div class="pane-stories collapsible collapsible-collapsed" data-collapsible-set="pp-stories-hottest">
<h3 class="hed-stories">Most Emailed</h3>
<ul class="list-plain list-stories">
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/the-irs-cashed-her-check-then-the-late-notices-started-coming" title="View this">The IRS Cashed Her Check. Then the Late Notices Started Coming.</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/i-dont-trust-the-people-above-me-riot-squad-cops-open-up-about-disastrous-response-to-capitol-insurrection" title="View this">“I Don’t Trust the People Above Me”: Riot Squad Cops Open Up About Disastrous Response to Capitol Insurrection</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/turbotax-just-tricked-you-into-paying-to-file-your-taxes" title="View this">Here’s How TurboTax Just Tricked You Into Paying to File Your Taxes</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/dying-on-the-waitlist" title="View this">Dying on the Waitlist</a></li>
<li><a href="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/article/a-federal-appeals-court-has-ruled-in-favor-of-releasing-nypd-discipline-records" title="View this">A Federal Appeals Court Has Ruled in Favor of Releasing NYPD Discipline Records</a></li>
</ul>
</div><!-- /end .pane-stories -->
</div><!-- /end .tabs -->
</div><!-- /end .module-content -->
</div><!-- /end .pp-module.module-popular -->
<!-- /2219821/Desktop_MedRec_2 -->
<div class="ad ad-article ad-300x250">
<div class="inner htl-ad" data-prebid="0x0:|960x0:Desktop_Medrec_2" data-refresh="viewable" data-refresh-secs="60" data-sizes="0x0:|960x0:1x1,300x250" data-unit="Desktop_Medrec_2"></div>
</div><!-- end .ad -->
</div><!-- end #level4.promos-2 -->
<!-- end .wrapper -->
<!-- end .content -->
<div class="modal collapsible collapsible-collapsed" id="modal-search">
<div class="collapsible-header close-btn">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-close-light"></use>
</svg>
<span class="a11y">Close this screen</span>
</div>
<!-- end .collapsible-header -->
<div class="collapsible-content content">
<form action="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/search" class="nav-site-search" method="get">
<label class="a11y hed-form hed-site-search">Search ProPublica:</label>
<div class="input-wrapper reverse">
<input class="text nav-site-search-text" name="qss" placeholder="Search ProPublica" type="search" value=""/>
<input type="submit" value="Search"/>
</div>
</form>
</div>
<!-- end .collapsible-content -->
</div>
<!-- end #modal-search -->
<div class="modal collapsible collapsible-collapsed" id="modal-republish">
<div class="collapsible-header close-btn">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-close-light"></use>
</svg>
<span class="a11y">Close this screen</span>
</div>
<!-- end .collapsible-header -->
<div class="collapsible-content content">
<div class="wrapper">
<h3>Republish This Story for Free</h3>
<p class="license"><a href="https://creativecommons.org/licenses/by-nc-nd/3.0/" rel="license">Creative Commons License (CC BY-NC-ND 3.0)</a></p>
<div class="republish-instructions">
<h3 class="title"></h3>
<p class="byline"></p>
<p class="intro">Thank you for your interest in republishing this story. You are are free to republish it so long as you do the following:</p>
<ul>
<li>You have to credit us. In the byline, we prefer “Author Name, ProPublica.” At the top of the text of your story, include a line that reads: “This story was originally published by ProPublica.” You must link the word “ProPublica” to the original URL of the story.</li>
<li>If you’re republishing online, you must link to the URL of this story on propublica.org, include all of the links from our story, including our newsletter sign up language and link, and use our <a href="/pixelping">PixelPing tag</a>.</li>
<li>If you use canonical metadata, please use the ProPublica URL. For more information about canonical metadata, <a href="https://support.google.com/webmasters/answer/139066?hl=en">refer to this Google SEO link</a>.</li>
<li>You can’t edit our material, except to reflect relative changes in time, location and editorial style. (For example, “yesterday” can be changed to “last week,” and “Portland, Ore.” to “Portland” or “here.”)</li>
<li>You cannot republish our photographs or illustrations without specific permission. Please contact <a href="/cdn-cgi/l/email-protection#d598b0b1bcb487bcb2bda1a695a5a7baa5a0b7b9bcb6b4fbbaa7b2"><span class="__cf_email__" data-cfemail="d39eb6b7bab281bab4bba7a093a3a1bca3a6b1bfbab0b2fdbca1b4">[email protected]</span></a>.</li>
<li>It’s okay to put our stories on pages with ads, but not ads specifically sold against our stories. You can’t state or imply that donations to your organization support ProPublica’s work.</li>
<li>You can’t sell our material separately or syndicate it. This includes publishing or syndicating our work on platforms or apps such as Apple News, Google News, etc.</li>
<li>You can’t republish our material wholesale, or automatically; you need to select stories to be republished individually. (To inquire about syndication or licensing opportunities, contact our Vice President of Business Development, <a href="/people/celeste-lecompte">Celeste LeCompte</a>.)</li>
<li>You can’t use our work to populate a website designed to improve rankings on search engines or solely to gain revenue from network-based advertisements.</li>
<li>We do not generally permit translation of our stories into another language.</li>
<li>Any website our stories appear on must include a prominent and effective way to contact you.</li>
<li>If you share republished stories on social media, we’d appreciate being tagged in your posts. We have official accounts for ProPublica and ProPublica Illinois on both Twitter (<a href="https://twitter.com/propublica">@ProPublica</a> and <a href="https://twitter.com/propublicail">@ProPublicaIL</a>) and Facebook.</li>
</ul>
</div>
<!-- end .republish-content -->
<div class="republish-copy">
<p>Copy and paste the following into your page to republish:</p>
</div>
<!-- end .republish-copy -->
</div>
<!-- end .wrapper -->
</div>
<!-- end .collapsible-content -->
</div>
<!-- end #modal-republish -->
<div class="modal collapsible collapsible-collapsed" id="nav-menu">
<div class="collapsible-header close-btn">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-close-light"></use>
</svg>
<span class="a11y">Close this menu</span>
</div>
<div class="collapsible-content">
<nav class="primary-nav">
<ul>
<li class="primary-nav-item news-apps"><a href="/newsapps/">Graphics &amp; Data</a></li>
<li class="primary-nav-item topics"><a href="/topics/">Topics</a></li>
<li class="primary-nav-item series"><a href="/series/">Series</a></li>
<li class="primary-nav-item videos"><a href="/video/">Videos</a></li>
<li class="primary-nav-item impact"><a href="/impact/">Impact</a></li>
</ul>
</nav><!-- end .primary-nav -->
<nav class="org-nav">
<ul>
<li class="org-nav-item main"><a href="/">ProPublica</a></li>
<li class="org-nav-item local-initiatives"><a href="/local-initiatives/">Local Initiatives</a></li>
<li class="org-nav-item data-store"><a href="/datastore/">Data Store</a></li>
</ul>
</nav><!-- end .org-nav -->
<nav class="action-nav">
<p>Follow Us:</p>
<ul>
<li class="action-nav-item social facebook">
<a href="https://www.facebookcorewwwi.onion/propublica">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-facebook"></use>
</svg>
<span class="cta">Like us on Facebook</span>
</a>
</li>
<li class="action-nav-item social twitter">
<a href="https://twitter.com/propublica">
<svg class="icon" height="24" width="24">
<use xlink:href="#icon-twitter"></use>
</svg>
<span class="cta">Follow us on Twitter</span>
</a>
</li>
</ul>
</nav><!-- end .action-nav -->
</div><!-- end .modal-inner -->
</div><!-- end .modal -->
<!-- jQuery/Legacy JS -->
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/js/public/assets/all.js"></script>
<script src="//assets.propub3r6espa33w.onion/static/prod/v4/js/main.3604a59f.js"></script>
<script src="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/js/public/assets/beacons.js"></script>
<script src="//connect.facebook.net/en_US/all.js"></script>
<div id="fb-root"></div>
<script>
            FB.init({
                appId: '229862657130557', // App ID
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true // parse XFBML
            });
    </script>
<!-- FOOTER -->
<footer role="contentinfo">
<div class="newsletter">
<div class="site-footer-inner">
<form action="https://signup.propublica.org/newsletter/turing" class="ajax-form subscribe-email" method="post">
<h2 class="pp-module-title" id="digest-title">Stay informed with the Daily Digest.</h2>
<div aria-describedby="digest-title" class="form-wrap" role="group">
<label class="a11y" for="pp-email-signup--full">Enter your email</label>
<input class="text subscribe-email-text" id="pp-email-signup--full" name="email" onfocus="recaptchaInit()" placeholder="Enter your email" type="email"/>
<input class="btn subscribe-email-btn g-recaptcha" id="newsletter-signup-footer" type="submit" value="Sign Up"/>
<input name="target" type="hidden" value="l/125411/2018-11-01/5vd35q"/>
<input name="stlist" type="hidden" value="The Daily Digest"/>
<input name="source" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/"/>
<input name="placement" type="hidden" value="site-wide-footer"/>
<input name="region" type="hidden" value=""/>
<input name="success_location" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/forms/thankyou"/>
<input name="error_location" type="hidden" value="//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/forms/error"/>
<input name="origin_location" type="hidden" value="https://v3-www.propublica.org/"/>
</div>
</form><!-- end .subscribe-email -->
</div>
</div><!-- end .newsletter -->
<div class="site-footer-inner">
<nav aria-labelledby="footer-nav" id="footer-links">
<h2 class="a11y" id="footer-nav">Site Navigation</h2>
<div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>Sections</h3>
<ul>
<li><a href="/">ProPublica</a></li>
<li><a href="/local-reporting-network/">Local Reporting Network</a></li>
<li><a href="/texas/">Texas Tribune Partnership</a></li>
<li><a href="/datastore/">The Data Store</a></li>
<li><a href="/electionland/">Electionland</a></li>
</ul>
</div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>Browse by Type</h3>
<ul>
<li><a href="/topics/">Topics</a></li>
<li><a href="/series/">Series</a></li>
<li><a href="/video/">Videos</a></li>
<li><a href="/newsapps/">News Apps</a></li>
<li><a href="/getinvolved/">Get Involved</a></li>
<li><a href="/nerds/">The Nerd Blog</a></li>
<li><a href="/atpropublica/">@ProPublica</a></li>
<li><a href="/events/">Events</a></li>
</ul>
</div>
</div>
<div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>Info</h3>
<ul>
<li><a href="/about/">About Us</a></li>
<li><a href="/leadership/">Board and Advisors</a></li>
<li><a href="/staff/">Officers and Staff</a></li>
<li><a href="/diversity/">Diversity</a></li>
<li><a href="/jobs/">Jobs</a> and <a href="/fellowships/">Fellowships</a></li>
<li><a href="/local-initiatives/">Local Initiatives</a></li>
<li><a href="/media-center/">Media Center</a></li>
<li><a href="/reports/">Reports</a></li>
<li><a href="/impact/">Impact</a></li>
<li><a href="/awards/">Awards</a></li>
<li><a href="/corrections/">Corrections</a></li>
</ul>
</div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>Policies</h3>
<ul>
<li><a href="/code-of-ethics/">Code of Ethics</a></li>
<li><a href="/advertising/">Advertising Policy</a></li>
<li><a href="/legal/">Privacy Policy</a></li>
</ul>
</div>
</div>
<div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>Follow</h3>
<ul>
<li><a href="/newsletters/">Newsletters</a></li>
<li><a href="/series/trump-inc/">Podcast</a></li>
<li><a href="https://itunes.apple.com/us/app/propublica/id355298887?mt=8">iOS</a> and <a href="https://play.google.com/store/apps/details?id=com.propublica&amp;hl=en">Android</a></li>
<li><a href="//feeds.propub3r6espa33w.onion/propublica/main">RSS Feed</a></li>
</ul>
</div>
<div class="collapsible collapsible-collapsed collapsible-expandwhenwide" data-collapsible-set="footer-links">
<h3>More</h3>
<ul>
<li><a href="/tips/">Send Us Tips</a></li>
<li><a href="/steal-our-stories/">Steal Our Stories</a></li>
<li><a href="https://www.propub3r6espa33w.onion">Browse via Tor</a></li>
<li><a href="/contact/">Contact Us</a></li>
<li><a href="https://donate.propublica.org/">Donate</a></li>
</ul>
</div>
</div>
</nav>
<div class="site-copyright">
<a class="logo" href="/">
<svg height="75" role="img" width="574">
<use xlink:href="#propublica-logo"></use>
<text class="a11y">ProPublica</text>
</svg>
</a>
<p class="slogan">Journalism in the Public Interest</p>
<small>© Copyright 2021 Pro Publica Inc.</small>
</div>
</div><!-- end .site-footer-inner -->
</footer>
<!-- anchors used by `aria-describedby` states -->
<div class="squelch" id="wayfinding">
<span id="current-site">Current site</span>
<span id="current-page">Current page</span>
</div>
<script data-delay="3" data-endpoint="e.assets.p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion//partials/newsletter-roadblock-big-story.html" data-stylesheet="//assets.propub3r6espa33w.onion/static/prod/v4/css/deploy/syndicated-newsletter.css" data-syndicate="" data-target="l/125411/2019-04-26/6m938v" src="//assets.propub3r6espa33w.onion/static/prod/v4/js/deploy/syndicated-newsletter-v1.1.0.js">
</script>
<!-- END FOOTER -->
<script src="https://www.google.com/recaptcha/api.js?onload=grecaptchaLoaded&amp;render=6LdI1rAUAAAAACI0GsFv-yRpC0tPF5ECiIMDUz2x"></script>
<script type="application/ld+json">{"@context":"http://schema.org","@graph":[{"@type":"WebPage","author":{"@id":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion#identity"},"copyrightHolder":{"@id":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion#identity"},"copyrightYear":"2019","creator":{"@id":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion#creator"},"dateModified":"2021-02-18T15:29:57-05:00","datePublished":"2019-10-31T13:02:00-04:00","description":"ProPublica is an independent, non-profit newsroom that produces investigative journalism in the public interest.","headline":"ProPublica — Investigative Journalism and News in the Public Interest","image":{"@type":"ImageObject","url":"//assets.propub3r6espa33w.onion/2017-pp-open-graph-1200x630.jpg"},"inLanguage":"en-us","mainEntityOfPage":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion/","name":"ProPublica — Investigative Journalism and News in the Public Interest","publisher":{"@id":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion#creator"},"url":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion"},{"@id":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion#identity","@type":"NGO","address":{"@type":"PostalAddress","addressCountry":"US","addressLocality":"13th Floor","addressRegion":"New York","postalCode":"N.Y. 10013","streetAddress":"155 Avenue of the Americas"},"description":"ProPublica is an independent, non-profit newsroom that produces investigative journalism in the public interest.","email":"info@propublica.org","name":"ProPublica","sameAs":["https://twitter.com/propublica","https://www.facebookcorewwwi.onion/propublica/","https://en.wikipedia.org/wiki/ProPublica","https://www.youtube.com/user/propublica","https://github.com/propublica","https://www.linkedin.com/company/propublica/","https://www.instagram.com/propublica","https://www.pinterest.ie/propublica","https://vimeo.com/propublica"],"telephone":"1-212-514-5250","url":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion"},{"@id":"#creator","@type":"Organization"},{"@type":"BreadcrumbList","description":"Breadcrumbs list","itemListElement":[{"@type":"ListItem","item":"//p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion","name":"Homepage","position":1}],"name":"Breadcrumbs"}]}</script>"""

import re
from bs4 import BeautifulSoup

#(?:[/][a-zA-Z0-9-]*?\.)
url_16 = re.compile(r'(?:[a-zA-Z0-9-][\.]*)*[a-zA-Z2-7]{16}\.onion(?:[/|:|#]+[a-zA-Z0-9-]*)*')  # , re.DOTALL | re.MULTILINE
url_56 = re.compile(r'(?:[a-zA-Z0-9-][\.]*)*[a-zA-Z2-7]{56}\.onion(?:[/|:|#]+[a-zA-Z0-9-]*)*')
urls = re.findall(url_16, data)
urls.extend(re.findall(url_56, data))

urls = list(set(urls))
for i in urls:
    print("URL:", i)
print("URLS: ", urls)
