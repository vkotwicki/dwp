import scrapy
from pymongo import MongoClient
from datetime import datetime
from scrapy.http import FormRequest
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import TimeoutError, TCPTimedOutError
from bs4 import BeautifulSoup

import re

import time


class GetOnions(scrapy.Spider):
    name = "go"

    def __init__(self):
        self.client = MongoClient('mongodb://localhost:27017/')
        self.db = self.client["DWProject"]["DW_URLs"]

    def start_requests(self):

        urls = {
            'https://www.thedarkweblinks.com/page/4/',
            'https://www.thedarkweblinks.com/page/16/',
            'https://www.thedarkweblinks.com/page/18/',
            'https://www.thedarkweblinks.com/page/6/',
            'https://www.thedarkweblinks.com/page/5/',
            'https://www.thedarkweblinks.com/page/8/',
            'https://www.thedarkweblinks.com/page/9/',
            'https://www.thedarkweblinks.com/page/10/',
            'https://www.thedarkweblinks.com/page/15/',
            'https://www.thedarkweblinks.com/page/17/',
        }

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        response = response.text
        soup = BeautifulSoup(response, "html.parser").get_text()
        print(soup)

        # Can find links BS4 can't
        url_16 = re.compile(r'[a-zA-Z2-7]{16}(?:[\.onion]*)')  # , re.DOTALL | re.MULTILINE
        url_56 = re.compile(r'[a-zA-Z2-7]{56}(?:[\.onion]*)')

        urls = re.findall(url_16, response)
        urls.extend(re.findall(url_56, response))

        urls = list(set(urls))

        for i in urls:
            print(i)


