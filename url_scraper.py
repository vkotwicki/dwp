###
# Onion Status checker
# ***If run with arg flag FIRST**, uses .onion URLs from .txt to gather initial list of URLs to
# ***populate the DB -> seperate file, checks DB
# ***- If out of URLs, asks for more from user

# Pulls URLs from DB and scraps them for more URLs which it then sends to the DB in batches (batch
# batch per site or until 100 URLs reached)
# Also pulls title and header information from sites

# Pulls DB info

# Attempts to connect to .onion site
# @ Returns Status Code

# Populates JSON
# @ STR site:           .onion site
# @ STR parent_site:    site URL came from, =N/A if None
# @ LIST data:          Titles and headers
# @ STR status:         status code
json = {
    'site': '',
    'status': ''
    'parent_site': '',
    'data': [],
    }


def add_to_JSON(p1):
    """
    add_to_JSON does ...

    :param p1: describe param p1
    :return: ...
    """


# Populates DB with URL info


# Starts scraper (??)
def __init__():
    pass

